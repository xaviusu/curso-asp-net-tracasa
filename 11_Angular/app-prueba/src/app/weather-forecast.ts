export class WeatherForecast {
    public date : string = "";
    public temperaturaC : number = 0;
    public temperaturaF : number = 0;
    public Summary : string = "";
}
