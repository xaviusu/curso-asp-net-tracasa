import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BindingsComponent } from './bindings/bindings.component';
import { DirectivasStructComponent } from './directivas-struct/directivas-struct.component';
import { PropiedadesInputComponent } from './propiedades-input/propiedades-input.component';
import { ServicioEjemploService } from './servicio-ejemplo.service';
import { UsuServicioComponent } from './usu-servicio/usu-servicio.component';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
  declarations: [
    AppComponent,
    BindingsComponent,
    DirectivasStructComponent,
    PropiedadesInputComponent,
    UsuServicioComponent
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
