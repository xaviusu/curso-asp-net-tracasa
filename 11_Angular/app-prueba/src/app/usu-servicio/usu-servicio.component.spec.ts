import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuServicioComponent } from './usu-servicio.component';

describe('UsuServicioComponent', () => {
  let component: UsuServicioComponent;
  let fixture: ComponentFixture<UsuServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsuServicioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsuServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
