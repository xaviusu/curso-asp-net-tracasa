import { Component, OnInit } from '@angular/core';
import { ServicioEjemploService } from '../servicio-ejemplo.service';

@Component({
  selector: 'app-usu-servicio',
  templateUrl: './usu-servicio.component.html',
  styleUrls: ['./usu-servicio.component.css']
})
export class UsuServicioComponent implements OnInit {

  constructor(public svrEj : ServicioEjemploService) {

  }

  ngOnInit(): void {
  }

  activarPorSrv () : void {
    this.svrEj.activar(true);
  }


}
