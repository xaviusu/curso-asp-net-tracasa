import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas-struct',
  templateUrl: './directivas-struct.component.html',
  styleUrls: ['./directivas-struct.component.css']
})
export class DirectivasStructComponent implements OnInit {
 
  contadorComp = 0; //Como declararlo con var de C#
  arrayDeTextos: Array<string>;
    constructor() {
    this.contadorComp = 1;
    this.arrayDeTextos = ["relleno", "cuajada", "tarta de cuajada", "bacalao ajoarriero"];
  }

  ngOnInit(): void {
  }

  alPulsarBoton(): void {
    this.contadorComp ++;
  }
}
