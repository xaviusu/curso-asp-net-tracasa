import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bindings',
  template:`
  <div>Mostrando valor propiedad con interpolación (variable interpolada)</div>
  {{(10+5) + propiedadClase} + " - " + contadorComp}
  <!-- Equivalente al \${expresion} de JS o C# -->
  <!-- Con los paréntesis vinculamos un evento HTML con un metodo TS de clase-->
  <input type="button" value="Aumentar" (click)="alPulsarBoton()"/>
  <p>Pulsado {{ contadorComp }} veces </p>`
})
export class BindingsComponent implements OnInit {

  propiedadClase : string;
  static contadorEstatico: number = 0;
  contadorComp = 0; //Como declararlo con var de C#
  arrayDeTextos: Array<string>;
  constructor() {
    this.propiedadClase = "...";
    BindingsComponent.contadorEstatico ++;
    this.contadorComp = 1;
    this.arrayDeTextos = ["relleno", "cuajada", "tarta de cuajada", "bacalao ajoarriero"];
  }

  ngOnInit(): void {
    this.propiedadClase = `ngOnInit es el  primer método
    del ciclo de vida del componenete (ejecutado ${BindingsComponent.contadorEstatico} veces)`;
  }

  alPulsarBoton(): void {
    this.contadorComp ++;
  }

}
