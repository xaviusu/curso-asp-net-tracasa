﻿using System;

namespace Ejemplo04_Excepciones
{
    class Program
    {
        static void FuncionRecursivaInfinita(int i)
        {
            if(i < 100)
            {
                FuncionRecursivaInfinita(i+1);
            }
        }
        static void Main(string[] args)
        {
            // Tres tipos de errores:
            // 1. De sintaxis (de compilador)
            // 2. Excepciones (errores en tiempo de ejecución)
            // 3. Errores lógicos (como bucle infinito)

            // Excepciones tipicas existentes en .NET (y en otros lenguajes)
            // Stack Overflow

            try
            {
                // Justo este erro es muy dificil de controlar
                FuncionRecursivaInfinita(200);
            } catch(Exception ex)
            {
                Console.Error.WriteLine("ERROR: " + ex.Message);
            }

            try
            {
                Console.WriteLine("Corvitiendo lo inconvertible");
                int.Parse("asdf");
                Console.WriteLine("Esta linea ni se ve");
            } catch(FormatException ex) 
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }

            try
            {
                Console.WriteLine("Convir" +
                    "tiendo lo inconvertible");
                int.Parse("asdf");
                Console.WriteLine("Esta linea ni se ve");
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }

            try
            {
                Console.WriteLine("Crear un objeto nulo y trata de usarlo");
                object nadaMenos = null;
                nadaMenos.ToString();

            }
            catch (NullReferenceException ex)
            {
                Console.Error.WriteLine("ERROR!! " + ex.Message);
            }

            try
            {
                throw new FormatException("Erro de formato por que me da la gana");
            } catch(FormatException ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

            try
            {
                FuncionQueDelegaExcepcion();
            } catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }

        }

        public static void FuncionQueDelegaExcepcion()
        {
            // Si una funcion lanza una excepcion, y no la controlamos aqui
            // podrá o debera ser controlada en la funcion que invoca a esta

            int.Parse("Error");
        }
    }
}
