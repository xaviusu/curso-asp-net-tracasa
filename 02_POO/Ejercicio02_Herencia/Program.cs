﻿using System;
using Ejercicio_01_Encapsulacion;

namespace Ejercicio02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario[] usuarios = new Usuario[4];
            Usuario usuario1 = new Usuario("hola", 23, 1.78f);
            Usuario usuario2 = new Usuario("hola", 24, 1.78f);
            Empleado usuario3 = new Empleado("hola", 25, 1.78f, 10000);
            Empleado usuario4 = new Empleado("hola", 26, 1.78f , 20000);
            
            usuarios[0] = usuario1; 
            usuarios[1] = usuario2; 
            usuarios[2] = usuario3; 
            usuarios[3] = usuario4; 

            foreach(Usuario usu in usuarios)
            {
                if(usu.GetType() == typeof(Empleado))
                {
                    ((Empleado)usu).SubirSalario();
                }
                Console.WriteLine(usu.ToString());
            }
        }
    }
}
