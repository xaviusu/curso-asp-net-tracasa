﻿
using Ejemplo_01_Encapsulacion;
using System;
using System.Collections.Generic;
using System.Text;
using Ejercicio_01_Encapsulacion;

namespace Ejercicio02_Herencia
{
    public class Empleado : Usuario, INombrable, IMetodos
    {
        float salario;
        Coche suCoche;
        public Empleado(string nombre, int edad, float altura, float salario) : base(nombre, edad, altura)
        {
            this.salario = salario;
            suCoche = null;
        }

        public Empleado()
        {
            this.salario = 0;
        }

        public Coche SuCoche
        {
            get { return suCoche; }
            set { suCoche = value; }
        }
        public float Salario
        {
            get
            {
                return salario;
            }
            set
            {
                this.salario = value;
            }
        }

        public void SubirSalario()
        {
            salario = salario + 10000;
        }

        public override string ToString()
        {
            return base.ToString() + " Salario: " + salario + (SuCoche == null ? "" : " Posee coche: " + suCoche.ToString()) ;
        }
        public override void MostrarDatos()
        {
            Console.WriteLine("Nombre: " + Nombre + " Edad: " + Edad + " Altura: " + Altura + " Salario: " + Salario);
        }

        public override void LeerDatos()
        {
            InterfazConsola.PedirTexto("Marca", out this.nombre);
            InterfazConsola.PedirEntero("Edad", out this.edad);
            InterfazConsola.PedirEntero("Altura", out this.altura);
            InterfazConsola.PedirEntero("Salario", out this.salario);

        }

    }
}
