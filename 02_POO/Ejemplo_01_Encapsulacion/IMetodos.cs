﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_01_Encapsulacion
{
    public interface IMetodos
    {
        void MostrarDatos();
        void LeerDatos();
    }
}
