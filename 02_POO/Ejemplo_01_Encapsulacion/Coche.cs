﻿using Ejemplo05_LibreriaClases;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo_01_Encapsulacion
{
    public class Coche : INombrable, IMetodos //Todas las clases heredan de Object
    { 
        protected int velocidad;
        protected string marca;
        protected string modelo;
        protected string nombre;

        public Coche()
        {
            Modelo = "";
            Velocidad = 0;
            Modelo = "";
        }

        public Coche(string marca, string modelo, int velocidad)
        {
            this.marca = marca;
            this.modelo = modelo;
            this.velocidad = velocidad;
        }

        public override bool Equals(Object obj)
        {
            Coche objCoche = (Coche)obj;
            Console.WriteLine(objCoche.Velocidad + objCoche.Marca + objCoche.Modelo);
            Console.WriteLine(Velocidad + Marca + Modelo);

            return this.Modelo == objCoche.Modelo && this.Marca.Equals(objCoche.Marca) && this.Velocidad == Velocidad;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override string ToString()
        {
            return "Marca: " + Marca + ", Modelo: " + Modelo;
        }

        //Mejor que getter y setters c# tiene las propiedades
        //Las propiedades como son metodos, empiezan por mayuscula
        public int Velocidad //Pero como se usan como variables, van sin paréntesis
        {
           get
            {
                return velocidad;
            }
            set
            {
                this.velocidad = value;
            }
        }

        public string Nombre
        {
            get
            {
                return GetNombre();
            }
            set
            {
                SetNombre(value);
            }
        }
        
       

        public string Marca
        {
            get
            {
                return this.marca;
            }
            set
            {
                this.marca = value;
            }
        }

        public string Modelo
        {
            get
            {
                return this.modelo;
            }
            set
            {
                this.modelo = value;
            }
        }

        string INombrable.Nombre { get => GetNombre(); set => SetNombre(value); }

        public virtual void Acelerar()
        {
            velocidad++;
        }

        public void SetNombre(string nombre)
        {
            if (string.IsNullOrEmpty(nombre))
            {
                this.nombre = nombre;
                Console.WriteLine("holi");
                /*string[] separados = nombre.Split("-");
                Marca = separados[0].Trim();
                if (separados.Length > 1)
                {
                   this.nombre = separados[1].Trim();
                }*/
            }

        }

        public string GetNombre()
        {
            return this.nombre;
        }

        public virtual void MostrarDatos()
        {
            Console.Write("Marca: " + Marca + ", Modelo: " + Modelo + " , Velocidad: " + Velocidad);
        }

        

        public virtual void LeerDatos()
        {
            InterfazConsola.PedirTexto("Marca", out this.marca);
            InterfazConsola.PedirTexto("Modelo", out this.modelo);
            InterfazConsola.PedirEntero<int>("Velocidad", out this.velocidad);
        }
    }
}
