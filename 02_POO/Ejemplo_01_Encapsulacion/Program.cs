﻿using Ejemplo05_LibreriaClases;
using System;

namespace Ejemplo_01_Encapsulacion
{
    public class Program
    {
        static void Main(string[] args)
        {

            Coche miCoche = new Coche();
            //miCoche.velocidad = 10 //Por defecto es privado, no podemos (ni debemos) modificar directamente
            miCoche.Marca = "Opel";
            //Console.WriteLine("Modelo: " + miCoche.Modelo);
            //Console.WriteLine("Marca: " + miCoche.Marca);
            Coche fiatPunto = new Coche("Fiat", "Punto", 100);
            INombrable fiatNombrable = (INombrable)fiatPunto;
            fiatNombrable.SetNombre("hola");
            fiatPunto.SetNombre("holi");
            Console.WriteLine(fiatPunto.GetNombre());
            
        }
    }
}
