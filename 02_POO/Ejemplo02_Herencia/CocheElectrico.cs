﻿using System;
using System.Collections.Generic;
using System.Text;
using Ejemplo_01_Encapsulacion;
using Ejemplo05_LibreriaClases;

namespace Ejemplo02_Herencia
{
    public class CocheElectrico : Coche, INombrable, ICloneable
    {
        double nivelBateria;
        
        public CocheElectrico()
        {
            this.nivelBateria = 100;
            Marca = "Una asiatica";

            Modelo = "Casero";
        }

        public CocheElectrico(double nivelBateria, string marca, string modelo, int velocidad) : base(marca, modelo, velocidad)
        {
            this.nivelBateria = 100;
        }
        // Sobrecarga de constructores
        

        public double NivelBateria
        {
            get
            {
                return nivelBateria;
            }
            set
            {
                this.nivelBateria = value;
            }
        }
        //base es como this, pero con la forma del padre, sirve para invocar a los metodos y variables del padre

        public override void Acelerar()
        {
            base.Acelerar();
            BajarNivel();
        }
        // Con new ignoramos que el metodo sea virtual, si es un metodo normal
        // es casi obligatorio usarlo.
        // Con override machacamos el metodo del padre y siempre usaremos el metodo del
        // hijo aunque el objeto tenga la forma del padre
        public override string ToString()
        {
            return "Coche electrico: " + base.Marca + base.Modelo + nivelBateria + " (" + base.ToString() + ")";
        }

        public void BajarNivel()
        {
            nivelBateria--;
        }

        public object Clone()
        {
            CocheElectrico cocheElectrico = new CocheElectrico(NivelBateria, Marca, Modelo, Velocidad);
            return cocheElectrico;
        }
        public override void MostrarDatos()
        {
            base.MostrarDatos();
            Console.WriteLine("Bateria: " + NivelBateria + "%");
        }

        public override void LeerDatos()
        {
            Ejemplo05_LibreriaClases.InterfazConsola.PedirTexto("Marca", out this.marca);
            Ejemplo05_LibreriaClases.InterfazConsola.PedirTexto("Modelo", out this.modelo);
            Ejemplo05_LibreriaClases.InterfazConsola.PedirEntero<int>("Velocidad", out this.velocidad);
            Ejemplo05_LibreriaClases.InterfazConsola.PedirEntero<double>("NivelBateria", out this.nivelBateria);
        }
    }
}
