﻿using System;
using Ejemplo_01_Encapsulacion;

namespace Ejemplo02_Herencia
{
    class Program
    {
        static void Main(string[] args)
        {
            Coche miCoche = new Coche();
            miCoche.Marca = "Opel";
            miCoche.Modelo = "Zafira";
            miCoche.Velocidad = 100;
            miCoche.Acelerar();
            Coche miCoche2 = new Coche();
            miCoche2.Marca = "Opel";
            miCoche2.Modelo = "Zafira";
            miCoche2.Velocidad = 100;
            miCoche2.Acelerar();
            Console.WriteLine("To string " + miCoche.ToString());
            Console.WriteLine("Hash code " + miCoche.GetHashCode());
            //miCoche2.Modelo = "Ibiza";
            if (miCoche.Equals(miCoche2))
            {
                Console.WriteLine("Son iguales");
            } else
            {
                Console.WriteLine("Son diferentes");
            }
            CocheElectrico miCocheElectrico = new CocheElectrico();
            miCocheElectrico.Marca = "Renault";
            miCocheElectrico.Modelo = "Clio";
            miCocheElectrico.Velocidad = 100;
            miCocheElectrico.NivelBateria = 100;
            Console.WriteLine(miCocheElectrico.NivelBateria);
            miCocheElectrico.Acelerar();
            Console.WriteLine(miCocheElectrico.NivelBateria);
            Console.WriteLine(miCocheElectrico.ToString());
            // Hacemos lo q se llama un casting implicito
            // usando el polimorfismo del lenguaje
            Coche miTeslaComoCoche = miCocheElectrico;
            // Lo que ocurre es que con la forma del padre solo podemos usar campos y metodos de la clase padre
            miTeslaComoCoche.Marca = "Tesla sencillo";
            // Aunque todavia almacene los datos de CocheElectrico no se pueden usar
            // ERROR: miTeslaComoCoche.nivelBateria = 100
            Object miTeslaComoObjeto = miCocheElectrico;
            Console.WriteLine(miTeslaComoObjeto.ToString());
            miTeslaComoCoche.Acelerar();
            miTeslaComoCoche.Acelerar();
            miTeslaComoCoche.Acelerar();
            Console.WriteLine(miTeslaComoCoche.ToString());

        }
    }
}
