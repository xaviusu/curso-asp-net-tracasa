﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo05_LibreriaClases
{
    public interface IMetodos
    {
        void MostrarDatos();
        void LeerDatos();
    }
}
