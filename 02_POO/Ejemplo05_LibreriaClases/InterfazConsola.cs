﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo05_LibreriaClases
{
    public class InterfazConsola
    {
        public static void PedirTexto(string nombreDato, out string varDato)
        {
            Console.Write(nombreDato + ": ");
            do
            {
                varDato = Console.ReadLine();
            } while (string.IsNullOrEmpty(varDato));
        }

        // Para pasar un tipo de dato definido por el usuario del metodo, estan los templates
        // o metodos genericos
        public static void PedirEntero<Tipo>(string nombreDato, out Tipo varDato)
        {
            Console.Write(nombreDato + ": ");
            bool esEntero = false;
            do
            {
                string str = Console.ReadLine();
                if (typeof(Tipo) == typeof(int))
                {
                    int entero;
                    esEntero = int.TryParse(str, out entero);
                    varDato = (Tipo)(object)entero;
                } else if(typeof(Tipo) == typeof(int))
                {
                    float numDecimal;
                    esEntero = float.TryParse(str, out numDecimal);
                    varDato = (Tipo)(object)numDecimal;
                }
                else if (typeof(Tipo) == typeof(double))
                {
                    double numDecimal;
                    esEntero = double.TryParse(str, out numDecimal);
                    varDato = (Tipo)(object)numDecimal;
                }
                else
                {
                    esEntero = true;
                    varDato = (Tipo)(object)0;
                }

                if(esEntero != true)
                {
                    Console.WriteLine("Escriba un numero");
                }
            } while (!esEntero);
        }
    }
}
