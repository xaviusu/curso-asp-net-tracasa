﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo05_LibreriaClases
{
    //Como un contrato que las clases que la implementen tienen que cumplir
    // Cualquier claswe que implemente la interfaz (NO se dice que herede)
    // debe tener programados todos sus metodos y propiedades
    public interface INombrable
    {
        // Al nombre de las interfaces se le suele añadir una I delante
        string GetNombre();
        
        void SetNombre(string nombre);

        string Nombre
        {
            get;
            set;
        } 
    }
}
