﻿using Ejemplo_01_Encapsulacion;
using Ejemplo05_LibreriaClases;
using System;

namespace Ejercicio_01_Encapsulacion
{
    public class Usuario : INombrable, Ejemplo05_LibreriaClases.IMetodos
    {
        protected string nombre;
        protected int edad;
        protected float altura;

        public Usuario(string nombre, int edad, float altura)
        {
            Nombre = nombre;
            Edad = edad;
            Altura = altura;
        }
        public Usuario()
        {

        }

        public override string ToString()
        {
            return "Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura;
        }

        public string GetNombre()
        {
            return nombre;
        }

        public void SetNombre(string nombre)
        {
            this.nombre = nombre;
        }
        public virtual void MostrarDatos()
        {
            Console.WriteLine("Nombre: " + nombre + " Edad: " + edad + " Altura: " + altura);
        }

        public virtual void LeerDatos()
        {
            Ejemplo05_LibreriaClases.InterfazConsola.PedirTexto("Marca", out this.nombre);
            Ejemplo05_LibreriaClases.InterfazConsola.PedirEntero("Edad", out this.edad);
            Ejemplo05_LibreriaClases.InterfazConsola.PedirEntero("Altura", out this.altura);
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }
            set
            {
                if(!String.IsNullOrEmpty(value))
                {
                    this.nombre = value;
                } else
                {
                    this.nombre = "SIN NOMBRE";
                }
            }
        }


        public int Edad
        {
            get
            {
                return edad;
            }
            set
            {
                if(value > 0)
                {
                    this.edad = value;
                } else
                {
                    this.edad = 1;
                }
            }
        }
        public float Altura
        {
            get
            {
                return altura;
            }
            set
            {
                if(value > 0.1f)
                {
                    this.altura = value;
                } else
                {
                    this.altura = 1f;
                }
            }
        }
    }
}
