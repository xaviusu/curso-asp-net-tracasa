﻿using System;


namespace Ejercicio_01_Encapsulacion
{
    public class Program
    {
        static void Main(string[] args)
        {
            Usuario usuario1 = new Usuario("", 0, 0f);
         
            Console.WriteLine("Nombre: " + usuario1.Nombre + " Altura: " + usuario1.Altura + " Edad: " + usuario1.Edad);
        }
    }
}
