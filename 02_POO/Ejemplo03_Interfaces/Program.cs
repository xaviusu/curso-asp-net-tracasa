﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ejemplo_01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio_01_Encapsulacion;

namespace Ejemplo03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {
            // Por lo general no es aconsejable crear un array de objetos
            // OJO: Esto es solo a nivel educativo
            // Lo normal es declarar un array de clases, interfaces o clases abstractas
            object[] popurri = new object[3];
            popurri[0] = new Coche("Fiat", "Punto", 9000);
            popurri[1] = new CocheElectrico(100, "Fiat", "Punto", 9000);
            popurri[2] = new Usuario("Xavier", 23, 1.80f);

            foreach (object objQueSea in popurri)
            {
                Console.WriteLine(objQueSea.ToString());
            }
            CocheElectrico cocheElectrico = new CocheElectrico(100, "Fiat", "Punto", 9000);
            cocheElectrico.SetNombre("HOLA");
            EjemploLista();
            
        }

        static void EjemploLista()
        {

            List<string> nuevaLista = new List<string>();
            // Por defecto se crear 10 o 20 elementos pero esto nos da igual
            // lo hace de manera interna
            nuevaLista.Add("Primer texto");
            nuevaLista.Add("Segundo texto");
            nuevaLista.Add("Tercer texto");
            nuevaLista.Add("Cuarto texto"); // Internamente va usando el array

            for(int i = 0; i < 30; i++)
            {
                nuevaLista.Add("Texto " + i + "º");
            }

            nuevaLista.RemoveAt(3);
            nuevaLista.Remove("Tercer texto");
            for(int i = 0; i<nuevaLista.Count; i++)
            {
                Console.WriteLine("Lista: " + nuevaLista[i]);
            }

            List<Coche> coches = new List<Coche>();
            coches.Add(new Coche("Toyota", "Cupra", 100));
            coches.Add(new Coche("Toyota", "Cupra2", 150));
            IList<Coche> icoches = coches;
            icoches.Add(new Coche("Toyota", "Cupra", 100));
            IList<Usuario> listaUsu = new List<Usuario>();
            listaUsu.Add(new Usuario("Hola", 10, 20));
            listaUsu.Add(new Usuario("Hola1", 10, 20));

            IList<Usuario> arrayUsu = new Usuario[10];
            arrayUsu[0] = new Usuario("Nuevo 1", 0, 0);
            arrayUsu[1] = new Usuario("Nuevo 2", 0, 0);
            

            MostrarColeccion(listaUsu);
            MostrarColeccion(arrayUsu);
        }

        static void MostrarColeccion(ICollection<Usuario> icollecion)
        {
            Console.WriteLine("Coleccion usuarios " + icollecion.GetType());
            
            foreach(Usuario usu in icollecion)
            {
                if(usu != null)
                {
                    usu.MostrarDatos();
                }

                /*try
                {
                    usu.MostrarDatos();
                } catch(Exception ex)
                {
                    Console.Error.WriteLine("ERROR: " + ex.Message);
                }*/

            }
        }

        
    }
}
