﻿using Ejemplo_01_Encapsulacion;
using Ejemplo02_Herencia;
using Ejercicio_01_Encapsulacion;
using Ejercicio02_Herencia;
using System;

namespace Ejercicio03_Interfaces
{
    class Program
    {
        static void Main(string[] args)
        {

            /*
            Empleado empleado = new Empleado("Xavier", 23, 1.80f, 10000);
            CocheElectrico cocheElectrico = new CocheElectrico(100, "Fiat", "Punto", 9000);
            INombrable[] nombrables = new INombrable[2];
            nombrables[0] = (INombrable)empleado;
            nombrables[1] = (INombrable)cocheElectrico;
            */
            object[] varios = new object[5];
            Usuario nuevoUsuario = new Usuario("Xavier", 23, 1.80f);
            Empleado nuevoEmpleado= new Empleado("Xavier1", 24, 1.81f, 2000);
            Coche nuevoCoche = new Coche("Opel", "Zafira", 100);
            CocheElectrico nuevoCocheElectrico = new CocheElectrico(100, "Renault", "Clio", 100);
            varios[0] = nuevoUsuario;
            varios[1] = nuevoEmpleado;
            varios[2] = nuevoCoche;
            varios[3] = nuevoCocheElectrico;
            CocheElectrico nuevoCocheElectrico2 = (CocheElectrico)((CocheElectrico)varios[3]).Clone();
            varios[4] = nuevoCocheElectrico2;

            //nuevoUsuario.LeerDatos();
            //nuevoEmpleado.LeerDatos();
            nuevoEmpleado.LeerDatos();
            //nuevoCocheElectrico.LeerDatos();
            ((Empleado)nuevoEmpleado).SuCoche = nuevoCoche;
            nuevoUsuario.MostrarDatos();
            nuevoEmpleado.MostrarDatos();
            nuevoCoche.MostrarDatos();
            Console.WriteLine();
            nuevoCocheElectrico.MostrarDatos();
            Console.WriteLine("==============");
            nuevoCocheElectrico2.MostrarDatos();

        }
    }
}
