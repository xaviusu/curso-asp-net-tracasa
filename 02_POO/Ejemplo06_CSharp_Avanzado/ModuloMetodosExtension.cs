﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_CSharp_Avanzado
{
    public static class ModuloMetodosExtension
    {

        public static string FormatearNombre(this Fruta laFruta)
        {
            string nombreFormateado = "Fruta " + laFruta.Nombre.ToUpper();

            return nombreFormateado;
        }


        //Mediante una función estatica, con esta estructura:
        public static string FormatearNombre(this Fruta laFruta, string formato)
        {
            string nombreFormateado = formato + " " + laFruta.Nombre.ToUpper();

            return nombreFormateado;
        }

        public static string ToUpperString(this object cualquierObjeto)
        {
            return cualquierObjeto.ToString().ToUpper();
        }

        public static string ToUpperString(this Array cualquierArray)
        {
            string result = "Array: " + cualquierArray.ToString().ToUpper();
            foreach (object obj in cualquierArray)
            {
                result = result + "\n - " + obj.ToUpperString();
            }
            return result;
        }

        public static string ToStringFiltrado(this Array cualquierArray, string cadena)
        {
            string result = "";
            foreach(object obj in cualquierArray)
            {
                if(obj.ToString().ToUpper().Contains(cadena.ToUpper()))
                {
                    result += obj.ToString();
                }
            }
            return result;
        }
    }

}
