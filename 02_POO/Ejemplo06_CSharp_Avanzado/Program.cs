﻿using System;

namespace Ejemplo06_CSharp_Avanzado
{
    class Program
    {
        static void Main(string[] args)
        {
            Fruta esLaPera = new Fruta("Pera", 150.1f);
            Fruta esLaManzana = new Fruta("Manzana", 120.1f);
            Fruta esElPlatano = new Fruta("Platano", 159.1f);

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(ModuloMetodosExtension.FormatearNombre(esLaPera));
            Console.WriteLine(ModuloMetodosExtension.FormatearNombre(esLaPera, "Fruta:"));
            Console.WriteLine(esLaPera.ToString());
            Console.WriteLine(esLaPera.ToUpperString());
            string[] otroObjeto = { "a", "b", "c", "d" };
            Console.WriteLine(otroObjeto.ToUpperString());
            Fruta[] arrayFrutas = new Fruta[3];
            arrayFrutas[0] = esLaPera;
            arrayFrutas[1] = esLaManzana;
            arrayFrutas[2] = esElPlatano;

            Console.WriteLine(arrayFrutas.ToUpperString());
            Console.WriteLine(arrayFrutas.ToStringFiltrado("platano"));

            Console.ForegroundColor = ConsoleColor.White;

        }
        
    }
    // En C# podemos añadir metodos a una clase desde otras clases:
    
}
