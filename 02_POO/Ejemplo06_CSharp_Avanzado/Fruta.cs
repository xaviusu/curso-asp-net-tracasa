﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_CSharp_Avanzado
{
    public class Fruta
    {
        string nombre;
        float pesoMedio;

        public Fruta(string nombre, float pesoMedio)
        {
            this.Nombre = nombre ?? throw new ArgumentNullException(nameof(nombre));
            this.PesoMedio = pesoMedio;
        }

        public float PesoMedio { get => pesoMedio; set => pesoMedio = value; }
        public string Nombre { get => nombre; set => nombre = value; }

        public override string ToString()
        {
            return "Fruta: " + Nombre + ", Peso medio: " + PesoMedio + " gr"; 
        }
    }
}
