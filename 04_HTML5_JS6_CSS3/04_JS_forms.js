window.onload = function () {

    let btnAnadir = document.getElementById("btn-anadir");
    
    let usuario = JSON.parse(window.localStorage.getItem("mi-usuario"));

    let filaTabla = `<tr><td>${usuario.nombre}</td><td>${usuario.edad}</td><td>${usuario.altura}</td></tr>`
    document.getElementById("tbody-usuarios").innerHTML = filaTabla;
    btnAnadir.onclick = function() {
        let campoNombre = document.getElementById("nombre");
        let campoEdad = document.getElementById("edad");
        let campoAltura = document.getElementById("altura");
        let nombre = campoNombre.value.toUpperCase();
        let edad = parseInt(campoEdad.value);
        let altura = parseFloat(campoAltura.value);
    
        let leer = document.getElementById("aficiones-leer");
        let musica = document.getElementById("aficiones-musica");
        let cine = document.getElementById("aficiones-cine");
        let aficiones = new Object();
        aficiones.musica = musica.checked;
        aficiones.cine = cine.checked;
        aficiones.leer = leer.checked;
        alert(JSON.stringify(usuario));
         
        textoAlertUsuario(usuario);
        window.localStorage.setItem("mi-usuario", JSON.stringify(usuario));
        //let sigEdad = parseInt(campoEdad.value) + 10;
        //alert(`Hola ${nombre} Tienes ${edad} y la siguiente decada tendrás ${sigEdad} !`);
    };
    function textoAlertUsuario(usu) {
        alert(`Hola ${usu.nombre} Tienes ${usu.edad} y altura ${usu.altura} y te gusta ${usu.aficiones.leer ? "leer" : ""} ${usu.aficiones.musica ? "la musica" : ""} ${usu.aficiones.cine ? " el cine" : ""}!`);
    }
    document.getElementById("btn-ir").addEventListener("click", function() {
        window.location = document.getElementById("url").value;
    });
};

