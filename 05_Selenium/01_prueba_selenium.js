const {Builder, By, Key, until} = require('C:/Users/pmpcurso1/Desktop/node-v14.17.6-win-x64/node-v14.17.6-win-x64/node_modules/selenium-webdriver');

(async function example() {
    let driver = await new Builder().forBrowser('chrome').build();
    try {
        // Navigate to Url
        await driver.get('https://duckduckgo.com/');

        // Enter text "cheese" and perform keyboard action "Enter"
        await driver.findElement(By.name('q')).sendKeys('webdriver', Key.ENTER);

        let firstResult = await driver.wait(until.elementLocated(By.css('h3')), 2000);

        console.log(await firstResult.getAttribute('textContent'));
    }
    finally{
        //driver.quit();
    }
})();