﻿using System;
using System.Collections.Generic;
using System.Text;


namespace ModeloUsuarios
{
    public class Vista : IVista
    {
        Controlador controlador;
        public Vista(Controlador controlador)
        {
            this.controlador = controlador;
        }

        public void Menu()
        {
            bool var = false;
            while (!var)
            {

                Console.WriteLine("MENU");
                Console.WriteLine("1.- Mostrar uno");
                Console.WriteLine("2.- Mostrar todo");
                Console.WriteLine("3.- Eliminar uno");
                Console.WriteLine("4.- Crear uno");
                Console.WriteLine("5.- Modificar uno");
                Console.WriteLine("6.- Salir");
                bool comprobar = int.TryParse(Console.ReadLine(), out int opcion);
                int entero;
                if (comprobar && (opcion >= 1 && opcion <= 6))
                {
                    switch (opcion)
                    {
                        case 1:
                            Console.WriteLine("Elige la posicion del Usuario");
                            entero = int.Parse(Console.ReadLine());
                            Console.WriteLine(LeerUno(entero));
                            break;
                        case 2:
                            Console.WriteLine("Usuarios: ");
                            foreach (Usuario usu in LeerTodos())
                            {
                                Console.WriteLine(usu.ToString());
                            }
                            break;
                        case 3:
                            Console.WriteLine("Elige la posicion del Usuario");
                            entero = int.Parse(Console.ReadLine());
                            bool eliminado = Eliminar(entero);
                            if(eliminado)
                            {
                                Console.WriteLine("Eliminado");
                            }
                            else
                            {
                                Console.WriteLine("Error, no eliminado");
                            }
                            break;
                        case 4:
                            Crear();
                            break;
                        case 5:
                            Modificar();
                            break;
                        case 6:
                            var = true;
                            break;
                    }
                }
                else
                    Console.WriteLine("Introduce un número válido");
            }

        }
        public void Crear()
        {
            Console.WriteLine("Escriba los datos de usuario");
            Console.WriteLine("Nombre");
            string nombre = Console.ReadLine();
            Console.WriteLine("Edad: ");
            int edad = int.Parse(Console.ReadLine());
            Console.WriteLine("Altura: ");
            float altura = float.Parse(Console.ReadLine());
            Usuario nuevoObj = new Usuario(nombre, edad, altura);
         
            controlador.Crear(nuevoObj);
        }

        public bool Eliminar(int entero)
        {
            return controlador.Eliminar(entero);
        }
        public IList<Usuario> LeerTodos()
        {
            return controlador.LeerTodos();
        }

        public Usuario LeerUno(int entero)
        {
            if (controlador.LeerUno(entero) == null)
            { 
                Console.WriteLine("No existe");
                return null;
            }
            else
                return controlador.LeerUno(entero);
        }
        public void Modificar()
        {
            Console.WriteLine("Introduce el nombre a modificar: ");
            string nombre = Console.ReadLine();
            if (controlador.LeerConfirmar(nombre) == false)
            {
                Console.WriteLine("No se ha encontrado " + nombre);
            }
            else
            {
                Console.WriteLine("Introduzca los datos del nuevo usuario: ");
                Console.WriteLine("Nombre: " + nombre);
                Console.Write("Edad: ");
                int edad = int.Parse(Console.ReadLine());
                Console.Write("Altura: ");
                float altura = float.Parse(Console.ReadLine());
                Usuario nuevoUsuario = new Usuario(nombre, edad, altura);                
                controlador.Modificar(nuevoUsuario);
            }
        }

        
    }
}
