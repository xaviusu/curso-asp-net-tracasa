﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    /* Como un contrato que las clases que la implementen tienen que cumplir
     Cualquier clase que implemente la interfaz (NO se dice que herede)
    debe tener programados todos sus métodos y propiedades)
    Las interfaces privadas (como las clases privadas) sólo se pueden
    usar dentro del proyecto (aplicación, librería DLL...)
    Si son públicas pueden usarse fuera del proyecto
 */
    public interface INombrable
    {
        string GetNombre();

        void SetNombre(string unNombre);

        string Nombre
        {
            get;
            set;
        }

        //  A priori no se debe, las interfaces por definición son contratos,
        // No clases
        void MetodoQUeSea()
        {
            Console.WriteLine("Hacer cosas");
        }
    }
}
