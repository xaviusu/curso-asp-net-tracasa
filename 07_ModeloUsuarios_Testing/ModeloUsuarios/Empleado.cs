﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace ModeloUsuarios
{
    public class Empleado : Usuario, INombrable, IEditableConsola
    {
        float salario;
        public Empleado(string nombre, int edad, float altura, float salario) : base(nombre, edad, altura)
        {
            this.salario = salario;
        }

        public Empleado()
        {
            salario = 0;
        }

        public float Salario
        {
            get
            {
                return salario;
            }
            set
            {
                salario = value;
            }
        }

        public void SubirSalario()
        {
            salario = salario + 10000;
        }

        public override string ToString()
        {
            return base.ToString() + " Salario: " + salario;
        }
        public override void MostrarDatos()
        {
            Console.WriteLine("Nombre: " + Nombre + " Edad: " + Edad + " Altura: " + Altura + " Salario: " + Salario);
        }

        public override void PedirDatos()
        {
            bool comprobar;
            Console.WriteLine("Nombre: ");
            Nombre = Console.ReadLine();

            do
            {
                Console.WriteLine("Edad: ");
                comprobar = int.TryParse(Console.ReadLine(), out int var);
                Edad = var;
            } while (!comprobar);


            do
            {
                Console.WriteLine("Altura: ");
                comprobar = float.TryParse(Console.ReadLine(), out float var);
                Altura = var;
            } while (!comprobar);
            do
            {
                Console.WriteLine("Salario: ");
                comprobar = float.TryParse(Console.ReadLine(), out float var);
                Salario = var;
            } while (!comprobar);

        }

    }
}
