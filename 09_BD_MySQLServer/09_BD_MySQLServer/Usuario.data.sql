﻿SET IDENTITY_INSERT [dbo].[Usuario] ON
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'asdf@gmail.com', N'asdf', 12, 2, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'ghjk@gmail.com', N'ghjk', 13, 19, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'qwer@gmail.com', N'qwer', 23, 1.2, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'xcv@gmail.com', N'xcv', 2, 1, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'dfcxv@gmail.com', N'xcasdv', 23, 1.3, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'hjygmn@gmail.com', N'bvgfxcv', 22, 1.6, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'jnvkj@gmail.com', N'xwercv', 12, 1.5, 0)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'dsiovoi@gmail.com', N'xregcv', 52, 1.34, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'iweri@gmail.com', N'xcvfv', 34, 1.76, 1)
INSERT INTO [dbo].[Usuario] ([email], [nombre], [edad], [altura], [activo]) VALUES (N'lkmfmn@gmail.com', N'xsacwe', 65, 1.88, 0)
SET IDENTITY_INSERT [dbo].[Usuario] OFF
