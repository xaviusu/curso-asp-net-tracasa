﻿-- Para evitar duplicados
-- SELECT DISTINCT edad FROM [dbo].[Usuario]
-- SELECT COUNT(DISTINCT edad) FROM [dbo].[Usuario]

-- SELECT * FROM [dbo].[Usuario] WHERE edad > 30
-- SELECT * FROM [dbo].[Usuario] WHERE activo = 'False'
-- SELECT * FROM [dbo].[Usuario] WHERE activo = 1 AND altura < 2
-- SELECT * FROM [dbo].[Usuario] WHERE activo = 1 OR altura < 2
-- SELECT * FROM [dbo].[Usuario] WHERE not(activo = 1 OR   altura < 2)


-- SELECT * FROM [dbo].[Usuario] ORDER BY edad DESC, activo ASC

-- SELECT * FROM [dbo].[Usuario] WHERE NOT(coche IS NULL)
-- SELECT * FROM [dbo].[Usuario] WHERE coche IS NOT NULL

--SELECT * FROM [dbo].[Usuario] 
--SELECT TOP 5 * FROM [dbo].[Usuario] WHERE coche is NULL ORDER BY nombre ASC;
--SELECT * FROM [dbo].[Usuario] 
--SELECT * FROM(

--TOP 5 CON MAS EDAD SIN COCHE ORDENADOS POR NOMBRE
--SELECT TOP 5 * FROM [dbo].[Usuario] WHERE coche is NULL ORDER BY edad DESC
--) AS subconsulta_5 ORDER BY nombre ASC

-- Numero activos y numero inactivos
--SELECT COUNT(DISTINCT activos.id), COUNT(DISTINCT inactivos.id) FROM (
--SELECT * FROM [dbo].[Usuario] WHERE activo = 'True')AS activos, (
--SELECT * FROM [dbo].[Usuario] WHERE activo = 'False')AS inactivos
-- OTRA MANERA
--SELECT COUNT(CASE activo when 1 then 1 end), COUNT(CASE activo when 0 then 0 end) FROM Usuario
-- Y OTRA
-- SELECT COUNT(activo) FROM  Usuario GROUP BY activo
-- Y OTRA MAS 
--SELECT COUNT(DISTINCT AC.ID)activos, COUNT(DISTINCT INAC.ID) inactivos
--FROM USUARIO AC, USUARIO INAC
--WHERE AC.activo = 1 AND INAC.activo = 0
-- Los tres mas bajitos ordenados por edad
--SELECT * FROM(
--SELECT TOP 3 * FROM [dbo].[Usuario] ORDER BY altura ASC
--) AS subconsulta_altura ORDER BY edad ASC

--Ejemplo german
--SELECT nombre, edad, case WHEN edad < 18 then 'Menor de edad' ELSE 'Mayor de edad' END FROM Usuario;

-- Devuelve los usuarios con el mismo coche
--SELECT * FROM Usuario AS U1, Usuario AS U2 WHERE U1.coche = U2.coche AND U1.id < U2.id

-- Devolver el usuario de mas edad con coche
--SELECT TOP 1 * FROM Usuario WHERE coche IS NOT NULL ORDER BY edad DESC

-- Devolver el usuario de menos edad inactivo y sin coche
-- SELECT TOP 1 * FROM Usuario WHERE coche is NULL AND activo = 'False' ORDER BY edad ASC

-- Prueba Joseba
--SELECT COUNT(*) AS cantidad, CASE WHEN coche IS NOT NULL THEN coche ELSE 'Sin coche' END FROM Usuario GROUP BY coche ORDER BY cantidad

-- Media edaddes de los usuarios activos, inactivos, con coche y sin coche
--SELECT AVG(edad) FROM Usuario WHERE activo = 'True'
--SELECT AVG(edad) FROM Usuario WHERE activo = 'False'
--SELECT AVG(edad) FROM Usuario WHERE coche IS NOT NULL
--SELECT AVG(edad) FROM Usuario WHERE coche IS NULL
--SELECT AVG(CASE WHEN activo = 1 THEN edad END), AVG(CASE WHEN activo = 0 THEN edad END), AVG(CASE WHEN coche IS NULL THEN edad END), AVG(CASE WHEN coche IS NOT NULL THEN edad END) FROM Usuario
--SELECT U.nombre FROM Usuario as U WHERE U.nombre LIKE 'Q%'


--CREATE INDEX [IX_Nombre_usuario] ON [dbo].[Usuario] (nombre);

--SELECT * FROM Usuario AS U,Coche AS C WHERE C.Id = U.idCoche
--SELECT * FROM Usuario AS U,Coche AS C WHERE C.Id = U.idCoche AND U.Edad > 50
--SELECT U.* FROM Usuario AS U,Coche AS C WHERE C.Id = U.idCoche AND U.Edad > 40
--SELECT SUM(C.Cantidad) FROM Usuario AS U,Producto AS P, CompraUsuPod AS C WHERE C.IdUsuario = U.Id AND P.Id = C.IdProducto AND U.edad <= 20