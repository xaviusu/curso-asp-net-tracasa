﻿--Usuarios con coche e ID y marca del coche
--SELECT U.id, U.email, C.id, C.Marca FROM Usuario AS U INNER JOIN Coche AS C ON U.idCoche = C.Id

--Todos los usuarios y si tienen coche el idCoche y la Marca, si no esos campos en NULL
--SELECT U.id, U.email, C.id, C.Marca FROM Usuario AS U LEFT JOIN Coche AS C ON U.idCoche = C.Id

--Todos los coches y usuarios q tengan esos coches además de los coches sin usuario con los campos de usuario null
--SELECT U.id, U.email, C.id, C.Marca FROM Usuario AS U RIGHT JOIN Coche AS C ON U.idCoche = C.Id

--Todos los coches y usuarios
--SELECT U.id, U.email, C.id, C.Marca FROM Usuario AS U FULL OUTER JOIN Coche AS C ON U.idCoche = C.Id

--CALCULAR LA SUMA DE CANTIDADES DE PRODUCTOS COMPRADOS POR PERSONAS MENORES DE 20 AÑOS
--SELECT SUM(C.Cantidad) FROM Usuario AS U,Producto AS P, CompraUsuPod AS C WHERE C.IdUsuario = U.Id AND P.Id = C.IdProducto AND U.edad <= 20
-- SUM(CompraUsuPod.Cantidad)
-- SELECT SUM(CompraUsuPod.Cantidad) FROM CompraUsuPod INNER JOIN Usuario AS U ON U.Id = CompraUsuPod.IdUsuario AND U.edad < 20 INNER JOIN Producto AS P ON P.Id = CompraUsuPod.IdProducto 

/* 
DECLARE @aux_id INT

DECLARE cur_u CURSOR FOR
	SELECT U.ID FROM Usuario AS U

OPEN cur_u

FETCH NEXT FROM cur_u INTO @aux_id

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT SUM(C.Cantidad), (SELECT Nombre FROM Usuario WHERE Usuario.id = @aux_id)
		FROM CompraUsuPod AS C WHERE C.IdUsuario = @aux_id

	FETCH NEXT FROM cur_u INTO @aux_id
END
CLOSE cur_u
DEALLOCATE cur_u
*/
/*Por cada usuario, nombre de usuario y nombre de producto,
2 - El precio del producto de sus compras
3 - Cuanto se ha gastado en total en cada producto
4 - Cuantas copias del producto ha comprado
Por otro lado:
5 - Todas las ventas (dinero) del 2020
6 - Todos los usuarios que compraron en el 2019
7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020
8 - El coche cuyo propietario ha comprado más a lo largo de la historia
9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
10 - Qué productos han comprado los usuarios del Mercedes*/

--1
/*
SELECT SUM(CompraUsuPod.Cantidad)
FROM CompraUsuPod 
INNER JOIN Usuario AS U ON U.Id = CompraUsuPod.IdUsuario
AND U.edad < 20 
INNER JOIN Producto AS P ON P.Id = CompraUsuPod.IdProducto 
*/

--2 - El precio del producto de sus compras
/*
SELECT Usuario.nombre, Producto.Nombre, Producto.Precio 
FROM CompraUsuPod 
INNER JOIN Usuario ON Usuario.Id = CompraUsuPod.IdUsuario
INNER JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto
*/

--3 - Cuanto se ha gastado en total en cada producto
--4 - Cuantas copias del producto ha comprado
/*
SELECT Usuario.nombre, Producto.Nombre, Producto.Precio, CompraUsuPod.Cantidad,(Producto.Precio*CompraUsuPod.Cantidad) as Gastado FROM CompraUsuPod INNER JOIN Usuario ON Usuario.Id = CompraUsuPod.IdUsuario
INNER JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto 
*/

--5 - Todas las ventas (dinero) del 2020
/*
SELECT SUM(Producto.Precio*CompraUsuPod.Cantidad)
FROM CompraUsuPod 
INNER JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto
WHERE YEAR(CompraUsuPod.Fecha)=2020
*/

--6 - Todos los usuarios que compraron en el 2019

/*
SELECT Usuario.Nombre 
FROM Usuario 
INNER JOIN CompraUsuPod ON Usuario.id = CompraUsuPod.IdUsuario 
WHERE YEAR(CompraUsuPod.Fecha)=2019 
*/

--7 - Qué productos se han vendido mejor a lo largo del 2018, 2019 y 2020

/*
SELECT subconsulta.Nombre, SUM(subconsulta.canti) as canti 
FROM (
	SELECT Producto.Nombre, (CompraUsuPod.Cantidad) AS canti
	FROM CompraUsuPod
	INNER JOIN Producto ON CompraUsuPod.IdProducto = Producto.Id
	INNER JOIN Usuario ON CompraUsuPod.IdUsuario = Usuario.Id
) AS subconsulta
GROUP BY subconsulta.Nombre
ORDER BY canti desc
*/

--8
/*SELECT TOP 1 subconsulta.modeloCoche 
FROM(
	SELECT SUM(CompraUsuPod.Cantidad) as suma,CompraUsuPod.IdUsuario as usu, 
	Usuario.nombre as nombre, Coche.Modelo AS modeloCoche 
	FROM CompraUsuPod
	INNER JOIN Usuario ON Usuario.Id = CompraUsuPod.IdUsuario
	INNER JOIN Coche ON Usuario.idCoche = Coche.Id
	GROUP BY CompraUsuPod.IdUsuario,Usuario.nombre,Coche.Modelo
) AS subconsulta
ORDER BY suma DESC
*/
	
--9 - La media de edad de los compradores de cada uno de los productos (melón, cuajada...)
/*
SELECT subconsulta.nombreProd, AVG(subconsulta.edad) AS Media 
FROM (
	SELECT DISTINCT Usuario.edad as edad, CompraUsuPod.IdProducto as idprod, Producto.Nombre AS nombreProd
	FROM Usuario  
	INNER JOIN CompraUsuPod ON Usuario.Id = CompraUsuPod.IdUsuario
	RIGHT JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto
) AS subconsulta 
GROUP BY subconsulta.nombreProd
*/

--10 - Qué productos han comprado los usuarios del Mercedes*/
 
/*
SELECT Usuario.nombre, Producto.Nombre FROM Usuario
INNER JOIN CompraUsuPod ON Usuario.Id = CompraUsuPod.IdUsuario
INNER JOIN Producto ON Producto.Id = CompraUsuPod.IdProducto
INNER JOIN Coche ON Usuario.idCoche = Coche.Id
WHERE Coche.Marca='Mercedes'
*/