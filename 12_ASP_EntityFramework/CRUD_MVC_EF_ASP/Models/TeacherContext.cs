﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD_MVC_EF_ASP.Models
{
    public class TeacherContext : DbContext
    {

        public TeacherContext(DbContextOptions<TeacherContext> options) : base(options)
        {

        }
        // Corresponde a una estructura tupo Set, cuya persistencia sera  en base de datos
        public DbSet<Teacher> Teachers { get; set; }

        //Metodo evento q va a ser llamado al configurarse la bbdd
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if(!optionsBuilder.IsConfigured)
            {
                Console.WriteLine("La bbdd NO ha sido configurada");
            } else
            {
                Console.WriteLine("La bbdd SI ha sido configurada");

            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Teacher>(
                entity =>
                {
                    entity  .Property(e => e.Name)
                            .IsRequired()
                            .HasMaxLength(50)
                            .IsUnicode(false);
                    entity.Property(e => e.Skills)
                            .IsRequired()
                            .HasMaxLength(100)
                            .IsUnicode(false);
                });
        }
    }
}
