﻿using Ejemplo03_MVC.DLL.Modelo;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC.Test
{
    class TestModeloEjemplo
    {
 
        [Test(Author = "XavierUsu", Description = "Probando modelo ejemplo")]
        public void ModeloEjemploTest()
        {
            IModeloEjemplo model1;
            model1 = new ModeloEjemploLista();
            model1.Crear(1, "Uno");
            model1.Crear(2, "Dos");
            model1.Crear(3, "Tres");
            Assert.AreEqual(model1.LeerTodos().Count, 3);
        }
    }
}
