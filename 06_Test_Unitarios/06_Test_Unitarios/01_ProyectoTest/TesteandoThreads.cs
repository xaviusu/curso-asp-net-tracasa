﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace _01_ProyectoTest
{
    class TesteandoThreads
    {
        [Test]
        public void TestThrowsAsync()
        {
            //Assert.ThrowsAsync<ArgumentException>(async () => MethodThatThrows());
            //Assert.Multiple(async () => await MethodThatThrows());
            Console.WriteLine("CINCO");
            
        }

        async Task MethodThatThrows()
        {
            Console.WriteLine("UNO");
            await Task.Delay(3000);
            Console.WriteLine("DOS");
            var t = Task.Run(delegate
            {
                Task.Delay(2000);
                Console.WriteLine("TRES");
            });
            Console.WriteLine("CUATRO");
            throw new Exception();
        }
        [Test]
        public void ProbandoHilos()
        {
            Console.WriteLine("Arrancando hasdf");
            Thread hilo1 = new Thread(FuncionHilo1);
            Thread hilo2 = new Thread(FuncionHilo2);
            hilo1.Start();
            hilo2.Start();
            Thread.Sleep(1000);
            Console.WriteLine("Terminando hasdf");
        }
        static int i = 0;

        public static void FuncionHilo1()
        {
            Console.WriteLine("Arrancando hilo 1");
            for (i = 0; i < 100000000; i++) ;
            Console.WriteLine("Terminando hilo 1" + i);
        }

        public static void FuncionHilo2()
        {

            Console.WriteLine("Arrancando hilo 2");
            for (i = 100000000; i > 0; i--) ;
            Console.WriteLine("Terminando hilo 2" + i);

        }
    }
}
