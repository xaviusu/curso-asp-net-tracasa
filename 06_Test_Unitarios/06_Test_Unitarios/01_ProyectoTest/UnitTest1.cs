using NUnit.Framework;
using System;

namespace _01_ProyectoTest
{
    public class Tests
    {
        string texto;
        // Este es el metodo de inicializaci�n porque lleva el atributo de C# [SetUp]
        // Los atributos son "decoradores", caracteristicas que a�adimos a clases, propiedades y metodos
        // Para agregar cierta funcionalidad

        [SetUp]
        public void Inicializacion()
        {
            texto = "hola";
        }

        [TearDown]
        public void Finalizacion()
        {
            texto = "Clean";
        }

        public static void DelegadoCualquieraOk()
        {
            Console.WriteLine("Delegado cualquiera ok");
        }

        public static void DelegadoCualquieraMal()
        {
            Console.WriteLine("Delegado cualquiera mal");
        }

        [Test]
        public void Test1()
        {
            // Assert = asegurar
            string asdf = null;
            Assert.Null(asdf);
        }

        [Test]
        public void Test2()
        {
            Assert.AreEqual(1 + 3, 2 + 2);
            Assert.AreNotEqual(1 + 4, 1);
            //Assert.AreEqual(texto, "Clean", "El texto no se ha limpiado correctamente");
            Assert.IsTrue(texto.Equals("hola"));
        }
        /*
        [Test]
        public void Test3()
        {
            Console.WriteLine("Antes del test 3");
            Assert.IsTrue(texto.Equals("hola"), "El texto no se ha inicializado");
            Assert.Contains('o', texto.ToCharArray(), "El array de char texto no tiene '�'");
            TestDelegate delegateOk = DelegadoCualquieraOk;
            Assert.DoesNotThrow(delegateOk, "Delegado cualquiera ok casco");
            Assert.Throws<NotImplementedException>(DelegadoCualquieraMal, "Delegado cualquiera mal casco");
            Console.WriteLine("Despues del test 3");
        }*/

        [Test(Author = "XavierUsu", Description = "Probando funcionalidad null and pass")]
        public void TestProbandoNullAndPass()
        {
            //Assert.Null("jjj","Holi", new object[1, 2] );
            Assert.Null(null);
            Assert.Pass("HOLA");
        }
    }
}