﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            int numero = 10;
            //Conversion implicita del la variable numero a string al concatenar con un string
            var str_num = "Un texto ";
            str_num  = 109.ToString();
            if(args.Length > 0)
            {
                Console.WriteLine("Hello World!" + args[0]);
            }
        }
    }
}
