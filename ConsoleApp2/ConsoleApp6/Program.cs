﻿using System;
using System.Numerics;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            Vector2[] vectordevectores = new Vector2[4];
            vectordevectores[0] = new Vector2(2,2);
            int[] vector = new int[4];
            Console.WriteLine("Escriba las coordenadas a escribir: ");
            int veces = int.Parse(Console.ReadLine());
            for(int i = 0; i<veces;i++)
            {
                Console.WriteLine("Escriba la coordenada x: ");
                int x = int.Parse(Console.ReadLine());
                Console.WriteLine("Escriba la coordenada y: ");
                int y = int.Parse(Console.ReadLine());
                if(x>0)
                {
                    if (y > 0)
                    {
                        vector[0]++;
                    } else if(y<0)
                    {
                        vector[3]++;
                    }
                } else if(x<0)
                {
                    if (y > 0)
                    {
                        vector[1]++;
                    }
                    else if(y<0)
                    {
                        vector[2]++;
                    }
                }
            }
            Console.WriteLine("Puntos primer cuadrante: " + vector[0]);
            Console.WriteLine("Puntos segundo cuadrante: " + vector[1]);
            Console.WriteLine("Puntos tercer cuadrante: " + vector[2]);
            Console.WriteLine("Puntos cuarto cuadrante: " + vector[3]);
        }
    }
}
