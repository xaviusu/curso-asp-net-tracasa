﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            string texto = "    En un lugar de la Mancha de cuyo nombre me quiero acordar, pero no me acuerdo    ";
            Console.WriteLine(texto);
            Console.WriteLine(texto.ToUpper());
            Console.WriteLine(texto.ToLower());
            Console.WriteLine("Un cacho hasta el final: " + texto.Substring(20));
            Console.WriteLine("Un cacho: " + texto.Substring(20,20));
            Console.WriteLine("Un cacho: " + texto.IndexOf("la Mancha"));
            Console.WriteLine("Un cacho: " + texto.IndexOf("Pamplona"));
            Console.WriteLine(texto.Trim());

            string[] palabras = texto.Trim().Split(" ");
            Console.WriteLine("Palabra por palabra: ");
            for(int i=0; i<palabras.Length; i++)
            {
                Console.WriteLine(palabras[i]);
            }
            Console.WriteLine(texto.Replace("la Mancha", "Pamplona").Replace("no","Sarriguren"));

            Console.WriteLine("Escriba tres palabras: ");
            String palabra1 = Console.ReadLine().Trim();
            String palabra2 = Console.ReadLine().Trim();
            String palabra3 = Console.ReadLine().Trim();
            string[] palabra = { palabra1, palabra2, palabra3 };
  
            for(int i=0; i<palabra.Length;i++)
            {
                if(palabra[i].Contains(" ")) {
                    palabra[i] = palabra[i].Insert(palabra[i].IndexOf(" "), "-").Replace(" ", "").Replace("-", " ");
                }
            }
            String linea = string.Join(",", palabra);
            Console.WriteLine(linea);

        }
    }
}
