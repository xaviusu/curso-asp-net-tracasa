﻿using System;

namespace Ejercicio_Struct_Class
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductoC productoC = new ProductoC("hola", 1);
            ProductoE productoE = new ProductoE("adios", 2);
            Console.WriteLine("AntesE: " + productoE.nombre + ", " + productoE.precio);
            Console.WriteLine("AntesC: " + productoC.nombre + ", " + productoC.precio);
            ModProdE(productoE);
            Console.WriteLine("ModProdE: " + productoE.nombre + ", " + productoE.precio);
            ModProdERef(ref productoE);
            Console.WriteLine("ModProdERef: " + productoE.nombre + ", " + productoE.precio);
            ModProdC(productoC);
            Console.WriteLine("ModProdC: " + productoC.nombre + ", " + productoC.precio);
            ModProdCRef(ref productoC);
            Console.WriteLine("ModProdCRef: " + productoC.nombre + ", " + productoC.precio);
        }

        static void ModProdE(ProductoE productoE)
        {
            Console.WriteLine("Escriba el nuevo nombre: ");
            productoE.nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            productoE.precio = float.Parse(Console.ReadLine());
        }
        static void ModProdERef(ref ProductoE productoE)
        {
            Console.WriteLine("Escriba el nuevo nombre: ");
            productoE.nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            productoE.precio = float.Parse(Console.ReadLine());
        }
        static void ModProdC(ProductoC productoC)
        {
            Console.WriteLine("Escriba el nuevo nombre: ");
            productoC.nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            productoC.precio = float.Parse(Console.ReadLine());
        }
        static void ModProdCRef(ref ProductoC productoC)
        {
            Console.WriteLine("Escriba el nuevo nombre: ");
            productoC.nombre = Console.ReadLine();
            Console.WriteLine("Escriba el nuevo precio: ");
            productoC.precio = float.Parse(Console.ReadLine());
        }

    }

    public struct ProductoE {
        public string nombre;
        public float precio;

        public ProductoE(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        void MostrarDatos()
        {
            Console.WriteLine("Nombre:  " + nombre + " Precio: " + precio);
        }
    }

    public class ProductoC
    {
        public string nombre;
        public float precio;

        public ProductoC(string nombre, float precio)
        {
            this.nombre = nombre;
            this.precio = precio;
        }

        void MostrarDatos()
        {
            Console.WriteLine("Nombre:  " + nombre + " Precio: " + precio);
        }
    }
}
