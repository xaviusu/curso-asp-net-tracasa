﻿using System;

namespace ConsoleApp8
{
    class Program
    {
        static void Main(string[] args)
        {
            Usuario usuario1 = new Usuario("Xavier", 23, (float) 1.80);
            Usuario usuario2 = new Usuario("Julen", 22, (float) 1.81);
            Usuario[] usuarios = new Usuario[4];
            usuarios[0] = usuario1;
            usuarios[1] = usuario2;
            Console.WriteLine("Escriba los datos de usuario");
            Console.Write("Nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("Edad: ");
            int edad = int.Parse(Console.ReadLine());
            Console.Write("Altura: ");
            float altura = float.Parse(Console.ReadLine());
            usuarios[2] = new Usuario(nombre, edad, altura);
            Console.WriteLine("Escriba los datos de usuario");
            Console.Write("Nombre: ");
            nombre = Console.ReadLine();
            Console.Write("Edad: ");
            edad = int.Parse(Console.ReadLine());
            Console.Write("Altura: ");
            altura = float.Parse(Console.ReadLine());
            usuarios[3] = new Usuario(nombre, edad, altura);
            for (int i =0; i<4; i++)
            {
                Console.WriteLine("Usuario " + (i + 1));
                usuarios[i].MostrarUsuario();
            } 
        }
        struct Usuario
        {
            string nombre;
            int edad;
            float altura;
            public Usuario(string nombre, int edad, float altura)
            {
                this.nombre = nombre;
                this.edad = edad;
                this.altura = altura;
            }
            public void MostrarUsuario()
            {
                Console.WriteLine("Nombre: " + nombre);
                Console.WriteLine("Edad: " + edad);
                Console.WriteLine("Altura: " + altura);
            }
        }
    }
}
