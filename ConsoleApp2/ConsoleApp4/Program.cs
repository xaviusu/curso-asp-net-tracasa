﻿using System;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = new int[5];
            string linea;
            for(int i=0; i < 5; i++)
            {
                Console.Write("Escribe el siguiente numero: ");
                linea = Console.ReadLine();
                nums[i] = int.Parse(linea);
            }
            int mayor = nums[0];
            int minimo = nums[0];
            
            for (int i=0; i < nums.Length; i++)
            {
             
                    if(nums[i] >  mayor)
                    {
                        mayor = nums[i];
                    }
                    if(nums[i] <  minimo)
                    {
                        minimo = nums[i];
                    }
            }
            Console.WriteLine("Maximo: " + mayor);
            Console.WriteLine("Minimo: " + minimo);
            Console.ReadKey();
        }
    }
}
