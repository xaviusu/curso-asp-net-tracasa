﻿using System;

namespace Ejercicio_Paso_Valor_Ref
{
    class Program
    {
        static void Main(string[] args)
        {
            int variableEnt = 10;

            Console.WriteLine("Entero fuera y antes: " + variableEnt);
            RecibimosUnaRef(ref variableEnt);
            Console.WriteLine("Entero fuera y después: " + variableEnt);
            string variableFrase = "Convertir en mayus";

            Console.WriteLine("Antes " + variableFrase);
            RecibimosStringRef(ref variableFrase);
            Console.WriteLine("Después " + variableFrase);

        }
        static void RecibimosUnValor(int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }

        static void RecibimosUnaRef(ref int entero)
        {
            Console.WriteLine("Entero dentro y antes: " + entero);
            entero = entero * 2 + 1;
            Console.WriteLine("Entero dentro y despues: " + entero);
        }
        static void RecibimosStringRef(ref string frase)
        {
            frase = frase.ToUpper();
        }
    }
}
