using NUnit.Framework;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace _08_NUnit_Selenium
{
    public class Tests
    {
        IWebDriver driver;
        [OneTimeSetUp]
        public void InicializarClaseTest()
        {
            string fichFirefox = "../../../../FirefoxPortable/FirefoxPortable.exe";
            if (!File.Exists(fichFirefox))
            {
                string instalador = "../../../../FirefoxPortable_92.0_English.paf.exe";
                if(File.Exists(instalador))
                {
                    System.Diagnostics.Process.Start(instalador);
                }
            }
            if(File.Exists(fichFirefox))
            {
                // string rutaDriverFirefox = "geckodriver.exe";
                // FirefoxBinary binarioFirefox = new FirefoxBinary(fichFirefox);
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.BrowserExecutableLocation = "../../../../FirefoxPortable/App/Firefox64/firefox.exe";
                driver = new FirefoxDriver(firefoxOptions);
            }
        }

        [OneTimeTearDown]
        public void FinalizarClaseTest()
        {
            //driver.Close();
        }


        [SetUp]
        public void Setup()
        {
        }
        /*
        [Test]
        public void Test1()
        {
            
            driver.Navigate().GoToUrl("https://duckduckgo.com/");

            IWebElement textoBusq = driver.FindElement(By.Name("q"));
            textoBusq.SendKeys("SQL Tutorials w3school create table");
            IWebElement botonBusq = driver.FindElement(By.Id("search_button_homepage"));
            botonBusq.Click();
            //var enlaces = driver.FindElements(By.CssSelector(".result__a"));
            var enlaces = driver.FindElements(By.CssSelector("a[href*='https://www.w3schools.com']")); 
            foreach(var enlace in enlaces)
            {
                if (!enlace.Equals(enlaces[1]) && enlace.Displayed)
                {
                    
                    driver.Navigate().GoToUrl(enlace.GetProperty("href"));
                    break;
                }
            }
            Assert.GreaterOrEqual(enlaces.Count, 3, "No se han encontrado enlaces");
            var btnCookies = driver.FindElement(By.Id("accept-choices"));
            btnCookies.Click();
            var enlaceSQL = driver.FindElement(By.CssSelector("a[href*='sql_datatypes.asp']"));
            //Actions actions = new Actions(driver);
            //actions.MoveToElement(enlaceSQL);
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", enlaceSQL);
            enlaceSQL.Click();
            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);
            IWebElement filas = driver.FindElement(By.XPath("/html/body/div[7]/div[1]/div[1]/div[4]/table/tbody/tr[1]"));
            String filas1 = driver.FindElement(By.XPath("/html/body/div[7]/div[1]/div[1]/div[4]/table/tbody/tr[1]/th")).Text;
            int numFilas = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[4]/table/tbody/tr")).Count;
            for(int i = 1; i < numFilas; i++)
            {
                Console.WriteLine(driver.FindElement(By.XPath("/html/body/div[7]/div[1]/div[1]/div[4]/table/tbody/tr[" + i + "]")).Text);
            }
            


        }
        */
        [Test]
        public void Test2()
        {
            driver.Navigate().GoToUrl("https://www.w3schools.com/sql/sql_datatypes.asp");
            var btnCookies = driver.FindElement(By.Id("accept-choices"));
            btnCookies.Click();
            IWebElement tituloNumDT = driver.FindElement(By.XPath("//h3[text()='Numeric Data Types'][2]"));
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].scrollIntoView(true);", tituloNumDT);
            var filas = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[1]"));
            var filas2 = driver.FindElements(By.XPath("/html/body/div[7]/div[1]/div[1]/div[8]/table/tbody/tr/td[3]"));
            Assert.AreEqual(filas.Count, filas2.Count, "No son del mismo tama�o");
            int i = 0;
            foreach(var fila in filas)
            {
                Console.WriteLine("Tipo: " + fila.Text + " , tama�o: " + filas2[i].Text);
                i++;
            }
            
        }
        public void Wait(int seg, int timeOut = 60)
        {
            var wait = new WebDriverWait(driver, new TimeSpan(0, 0, 1, timeOut));
            var delay = new TimeSpan(0, 0, 0, seg);
            var timeIni = DateTime.Now;
            wait.Until(driver => (DateTime.Now - timeIni) > delay);
        }
    }
}