﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejercicio06_Callback_Delegado
{


    static class PedirDatos
    {

        public static (float[],int,float) CalculaMovida(Func<float[], int, float> result)
        {
            float[] numeros = PedirNums();
            int eleccion = PedirOperando();
            Console.ForegroundColor = ConsoleColor.Green;

            return (numeros, eleccion, result(numeros, eleccion));
        }
        static float[] PedirNums()
        {

            Console.WriteLine("Escribe el numero de elementos");
            int len;
            bool esNum = int.TryParse(Console.ReadLine(), out len);
            if (esNum)
            {
                float[] arrayNums = new float[len];
                for (int i = 0; i < len; i++)
                {
                    Console.WriteLine("Numero: ");
                    arrayNums[i] = float.Parse(Console.ReadLine());
                }
                return arrayNums;
            }
            else
            {
                Console.WriteLine("ERROR: Introduce un numero");
                return null;
            }
        }

    
        static int PedirOperando()
        {
            Console.WriteLine("Eliga el operando: ");
            Console.WriteLine("1. Sumar ");
            Console.WriteLine("2. Restar ");
            Console.WriteLine("3. Multiplicar ");
            Console.WriteLine("4. Dividir");
            int elec = int.Parse(Console.ReadLine());
            switch(elec)
            {
                case 1:
                    return 1;
                case 2:
                    return 2;
                case 3:
                    return 3;
                case 4:
                    return 4;
            }
            return 0;
        }

    }
}
