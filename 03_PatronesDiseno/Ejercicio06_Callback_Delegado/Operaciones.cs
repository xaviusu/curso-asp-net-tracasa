﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Ejercicio06_Callback_Delegado
{
    static class Operaciones
    {
        public static float ElecOper(float[] nums, int elec)
        {
            switch(elec)
            {
                case 1:
                    return Operacion(nums, (float x, float y) => x + y);
                case 2:
                    return Operacion(nums, (float x, float y) => x - y);
                case 3:
                    return Operacion(nums, (float x, float y) => x * y);
                case 4:
                    return Operacion(nums, (float x, float y) => x / y);
            }
            return 0;
        }
        
        static float Operacion(float[] nums, Func<float, float, float> operar)
        {
            if(nums == null || nums.Length == 0)
            {
                throw new Exception("El array no puede estar vacio");
            }
            if(nums.Length > 1)
            {
                float res = 0;
                for(int i = 0; i < nums.Length; i++)
                {
                    if(i == 0)
                    {
                        res = nums[i];
                    } else {
                        res = operar(res, nums[i]);
                    }
                }
                return res;
            } else
            {
                return nums[0];
            }
        }
       
    }
}
