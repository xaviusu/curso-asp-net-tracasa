﻿using System;
using System.Linq;
namespace Ejercicio06_Callback_Delegado
{
    class Program
    {

        static void Main(string[] args)
        {
            string[] strs = { "uno", "dos", "tres" };
            Func<string, string, string> funSeparaComas = (string acum, string v) =>
              {
                  return acum + ", " + v;
              };
            Console.WriteLine(strs.Aggregate(funSeparaComas));


            var (numeros, elec, resultado) = PedirDatos.CalculaMovida(Operaciones.ElecOper);
            string formula = " " + numeros[0];
            char operando = ' ';
            switch(elec)
            {
                case 1:
                    operando = '+';
                    break;
                case 2:
                    operando = '-';
                    break;
                case 3:
                    operando = '*';
                    break;
                case 4:
                    operando = '/';
                    break;
            }
            for (int i = 1; i < numeros.Length; i++)
            {
                formula = formula + operando + numeros[i];
            }
            Console.Write("El resultado de: {0} es {1}", formula, resultado);
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
