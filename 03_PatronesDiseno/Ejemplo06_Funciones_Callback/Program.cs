﻿using System;

namespace Ejemplo06_Funciones_Callback
{

    //Creamos un nuevo tipo de dato, que indica que es una función
    //(ni clase, ni interfaz...) Donde lo que importa son los tipos de 
    //datos que recibe y que devuelve.
    // Es decir, declaramos un delegado.
    delegate float FuncionSuma(float op1, float op2);
    delegate float FuncionMultiplicar(float op1, float op2);

    //Ahora podremos crear variables de tipo
    // float función(float,float)
    class Program
    {
        static void Main(string[] args)
        {
            VistaOperadora(Calculadora_B.Suma, Calculadora_B.Multiplicar);
        }

        // Esta funcion necesita de una funcion callback para 
        // saber como tiene que calcular.


        static void VistaOperadora(FuncionSuma suma, FuncionMultiplicar mult)
        {
            Console.WriteLine("Introduzca la operacion a realizar sin espacios");
            string operacion = Console.ReadLine();
            if(operacion.Contains("*"))
            {
                float num1 = float.Parse(operacion.Split("*")[0]);
                float num2 = float.Parse(operacion.Split("*")[1]);
                Console.WriteLine(mult(num1,num2));
            } else if(operacion.Contains("+"))
            {
                float num1 = float.Parse(operacion.Split("+")[0]);
                float num2 = float.Parse(operacion.Split("+")[1]);
                Console.WriteLine(suma(num1,num2));
            } else
            {
                Console.WriteLine("No te entiendo, humano");
            }
        }
    }
}
