﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_Funciones_Callback
{
    static class Calculadora_A
    {
  
        public static float Suma(float x, float y)
        {
            return x + y;
        }

        public static float Multiplicar(float x, float y)
        {
            float res = 0;
            for (int i = 0; i < y; i++)
            {
                res += x;
            }
            return res;
        }
    }
}
