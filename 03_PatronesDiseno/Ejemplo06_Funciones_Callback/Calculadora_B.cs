﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo06_Funciones_Callback
{
    class Calculadora_B
    {


        public static float Suma(float x, float y)
        {
            return x + y;
        }

        public static float Multiplicar(float x, float y)
        {
            return x * y;
        }
    }
}
