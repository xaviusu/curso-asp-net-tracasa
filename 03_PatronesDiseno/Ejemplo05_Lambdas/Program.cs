﻿using System;

namespace Ejemplo05_Lambdas
{

    delegate void FuncionQueRecibeInt(int param);

    class Program
    {
        static void Main(string[] args)
        {
            //LAMBDAS
            //Funciones estaticas:
            FuncionEstatica(5);
            OtraEstatica(7);
            //Variables de tipo funcion
            FuncionQueRecibeInt funcRecInt;
            funcRecInt = FuncionEstatica;
            funcRecInt(200);
            funcRecInt = null;
            funcRecInt = OtraEstatica;
            funcRecInt(100);
            OtroSistema(OtraEstatica);
        }

        static void OtroSistema(FuncionQueRecibeInt funExt)
        {
            //Queremos recibir una funcion como parametro
            Console.WriteLine("Otro sistema hace sus cosas");
            System.Threading.Thread.Sleep(1000);
            Console.WriteLine("Tardad su tiempo");
            Console.WriteLine("Llamar a la funcionalidad externa");
            funExt(3);
            
            //int valor = 3;
            //LlamarFuncionExterna(FuncionEstatica());
        }

        static void FuncionEstatica(int x)
        {
            Console.WriteLine("No necesito un objeto para se llamada");
            Console.WriteLine("Param: " + x);

        }
        
        static void OtraEstatica(int y)
        {
            Console.WriteLine(y + " - Otra estatica: ");


        }

    }
}
