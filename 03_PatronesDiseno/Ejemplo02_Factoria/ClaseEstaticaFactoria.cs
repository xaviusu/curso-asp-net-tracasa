﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo02_Factoria
{
    static class ClaseEstaticaFactoria
    {
        static public Producto Crear(int id)
        {
            string nombre;
            switch (id)
            {
                case 1:
                    nombre = "Uno";
                    break;
                case 2:
                    nombre = "Dos";
                    break;
                case 3:
                    nombre = "Tres";
                    break;
                case 4:
                    nombre = "Cuatro";
                    break;
                default:
                    nombre = "Otro";
                    break;
            }
            return new Producto(id, nombre);
        }

        static public List<Producto> CrearLista(int inicio, int final)
        {
            List<Producto> nuevaLista = new List<Producto>();
            for (int i = inicio; i <= final; i++)
            {
                nuevaLista.Add(Crear(i));
            }
            return nuevaLista;
        }

        static public List<Producto> CrearLista(int final)
        {
            return CrearLista(1, final);
        }

        static public List<Producto> CrearLista(int[] ids)
        {
            List<Producto> lista = new List<Producto>();
            for (int i = 0; i < ids.Length; i++)
            {
                lista.Add(Crear(ids[i]));
            }
            return lista;
        }

        static public Producto Crear(int id, string nombre)
        {
            return new Producto(id, nombre);
        }
    }
}
