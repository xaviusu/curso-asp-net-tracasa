﻿using System;
using System.Collections.Generic;

namespace Ejemplo02_Factoria
{
    // https://refactoring.guru/es/design-patterns/factory-method/csharp/example

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Vamos a la fabrica!");
            FactoriaProducto fabrica = new FactoriaProducto();

            List<Producto> lista = fabrica.CrearLista(1, 4);
            List<Producto> lista2 = ClaseEstaticaFactoria.CrearLista(5);
            List<Producto> lista3 = fabrica.CrearLista(new int[] { 7,3,4,2});

            foreach (var item in lista)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }
            foreach (var item in lista2)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }
            foreach (var item in lista3)
            {
                Console.WriteLine("Objeto de constructor: " + item);
            }

        }
    }
}
