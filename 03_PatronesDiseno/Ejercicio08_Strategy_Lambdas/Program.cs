﻿using System;
using System.Collections.Generic;
using System.Linq;
using OtroNameSpace;

namespace Ejercicio08_Strategy_Lambdas
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("A");
            StrategyBase estrategiaA = AddConcreteStrategyA();
            estrategiaA.execute();
            Console.WriteLine("B");
            StrategyBase estrategiaB = AddConcreteStrategyB();
            estrategiaB.execute();
            Console.WriteLine("C");
            StrategyBase estrategiaC = AddConcreteStrategyC("Nuevo nombre");
            estrategiaC.execute();

        }
        static StrategyBase AddConcreteStrategyA()
        {
            StrategyBase strObj= new StrategyBase("Eeeeeeeoh");
            strObj.Type = "ConcreteStrategyA";
            strObj.execute = () =>
            {
                var baseMostrarNombre = strObj.mostrarNombre;
                strObj.mostrarNombre = () => 
                {
                    strObj.RepetirChar('_', 30);
                    baseMostrarNombre();
                    strObj.RepetirChar('_', 30);
                };
                strObj.mostrarNombre();
                strObj.RepetirChar('-', 30);
            };

            return strObj;
        }

        static StrategyBase AddConcreteStrategyB()
        {
            StrategyBase strObj = new StrategyBase("Llamar a");

            strObj.Type = "ConcreteStrategyB";

            strObj.execute = () =>
            {
                strObj.RepetirChar('\n', 3);
                strObj.mostrarNombre();
            };
            return strObj;
        }

        static StrategyBase AddConcreteStrategyC(string nombre)
        {
            StrategyBase strObj = new StrategyBase();

            strObj.Type = "ConcreteStrategyC";
            strObj.execute = () =>
            {
                strObj.RepetirChar('*', 20);
                strObj.GetNombre = () =>
                {
                    return nombre;
                };
                strObj.mostrarNombre();

            };
            return strObj;
        }

    }
}
