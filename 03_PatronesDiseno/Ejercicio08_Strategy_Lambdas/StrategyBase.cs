﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OtroNameSpace
{

    public delegate void ExecuteFun();
    public delegate void MostrarNombreFun();
    public class StrategyBase
    {
        public string nombre;
        public Func<string> GetNombre;
        public StrategyBase(string mensaje)
        {
            this.GetNombre = () =>
            {
                return mensaje + " " + GetType() + ".Execute";
            };
            //nombre = mensaje + " " + GetType() + ".Execute";
            mostrarNombre = () => Console.WriteLine(this.GetNombre());
        }

        public StrategyBase() : this("MENSAJE")
        {


        }

        public void RepetirChar(char caracter, int veces)
        {
            for (int i = 0; i < veces; i++)
            {
                Console.Write(caracter);
            }
            Console.WriteLine();
        }


        public MostrarNombreFun mostrarNombre;


        public ExecuteFun execute;

        string type;

        public string Type { get => type; set => type = value; }

        new string GetType()
        {
            return Type;
        }

    }
}
