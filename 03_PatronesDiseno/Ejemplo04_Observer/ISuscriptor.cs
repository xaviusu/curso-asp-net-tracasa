﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    interface ISuscriptor
    {
        void ActualizarNotificacionNoticia(string noticia, DateTime fecha);
        void ActualizarNotificacionNoticiaCorazon(string noticia, DateTime fecha);

    }
}
