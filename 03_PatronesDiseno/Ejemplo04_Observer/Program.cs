﻿using System;

namespace Ejemplo04_Observer
{
    class Program
    {
        static void Main(string[] args)
        {

            MovidasObserver();
            System.GC.Collect();
            System.Threading.Thread.Sleep(1000);
            
        }

        static void MovidasObserver()
        {
            Console.WriteLine("Praton observer: Periodico");
            PeriodicoObservado alDia = new PeriodicoObservado();
            // 1 - Un suscriptor humano
            SuscriptorHumano juan = new SuscriptorHumano("Juan");
            alDia.AddSuscriptor(juan);
            // 2 - Ocurre una noticia
            alDia.NotificarNoticia("Un meteorito roza la Luna", DateTime.Now);
            // 3 - Otro suscriptor humano
            SuscriptorHumano xavier = new SuscriptorHumano("Xavier");
            alDia.AddSuscriptor(xavier);
            // 4 - Otro suscriptor maquina
            SuscriptorMaquina maquina1 = new SuscriptorMaquina(0);
            alDia.AddSuscriptor(maquina1);
            // 5 - Ocurre otra noticia
            System.Threading.Thread.Sleep(2000);
            alDia.NotificarNoticia("Un meteorito roza la Tierra", DateTime.Now);
            alDia.NotificarNoticiaCorazon("Corazon", DateTime.Now);
            // 6 - El otro humano se desuscribe porque dice mucha mentira
            alDia.RemoveSuscriptor(xavier);
            // 7 - Ocurre la ultima noticia
            //alDia.NotificarNoticia("HOLA", DateTime.Now);
            // 8 - El periodico cierra
            //alDia = null;

            //Para q el suscriptor de ciencia se pueda suscribir,
            // se asigna la función al campo delegado
            alDia.NuevaNoticiaCiencia = NationalGeo;

            alDia.NuevaNoticiaCiencia("El meteorito existia y era de kriptonita");
        }
        //Este suscriptor solo es una funcion
        public static void NationalGeo(string noticiaPseudociencia)
        {
            Console.WriteLine(noticiaPseudociencia);
        }
    }
}
