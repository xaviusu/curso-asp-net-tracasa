﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorMaquina : ISuscriptor
    {
        int identificador;
        public SuscriptorMaquina(int identificador)
        {
            this.identificador = identificador;
        }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha )
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("¡¡NUEVAS NOTICIAS!! " + this.identificador);
            Console.WriteLine(noticia);
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("ficha_maquina.txt", '\n' + fecha.ToString("R") + " --> " + noticia );

        }

        public void ActualizarNotificacionNoticiaCorazon(string noticia, DateTime fecha)
        {
            Console.WriteLine("NO");
        }
    }
}
