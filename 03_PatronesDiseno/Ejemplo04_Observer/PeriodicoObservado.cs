﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    delegate void NoticiaCiencia(string not);
    class PeriodicoObservado
    {
        IList<ISuscriptor> listaSuscriptores;
        public NoticiaCiencia NuevaNoticiaCiencia;

        public PeriodicoObservado()
        {
            listaSuscriptores = new List<ISuscriptor>();
        }

        public void NoticiaCiencia(string laNoticia)
        {
            if (NuevaNoticiaCiencia != null)
            {
                NuevaNoticiaCiencia(laNoticia);
            }
        }

        public void NotificarNoticia(string titular, DateTime fecha)
        {
            Console.WriteLine("¡¡EXTRA, EXTRA!!");
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(fecha + " -> " + titular);
            foreach (ISuscriptor suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticia(titular, fecha);
            }
        }
        public void NotificarNoticiaCorazon(string titular, DateTime fecha)
        {
            Console.WriteLine("¡¡EXTRA, EXTRA!!");
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine(fecha + " -> " + titular);
            foreach (ISuscriptor suscriptor in listaSuscriptores)
            {
                suscriptor.ActualizarNotificacionNoticiaCorazon(titular, fecha);
            }
        }

        public void AddSuscriptor(ISuscriptor suscriptor)
        {
            listaSuscriptores.Add(suscriptor);
        }

        public void RemoveSuscriptor(ISuscriptor suscriptor)
        {
            listaSuscriptores.Remove(suscriptor);
        }

        ~PeriodicoObservado()
        {
            NotificarNoticia("ALDIA CIERRA SUS PUERTAS", DateTime.Now);
            listaSuscriptores.Clear();
         
        }
        
        
    }
}
