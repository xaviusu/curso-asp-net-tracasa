﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo04_Observer
{
    class SuscriptorHumano : ISuscriptor
    {
        string nombre;

        public SuscriptorHumano(string nombre)
        {
            this.nombre = nombre;
        }

        public string Nombre { get => nombre; set => nombre = value; }

        public void ActualizarNotificacionNoticia(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("¡¡NUEVAS NOTICIAS!! " + this.nombre);
            Console.WriteLine(fecha + " --> " + noticia);
            Console.ForegroundColor = ConsoleColor.White;
        }
        public void ActualizarNotificacionNoticiaCorazon(string noticia, DateTime fecha)
        {
            Console.Beep();
            Console.ForegroundColor = ConsoleColor.Magenta;
            Console.WriteLine("¡¡NUEVAS NOTICIAS!! " + this.nombre);
            Console.WriteLine(noticia);
            Console.ForegroundColor = ConsoleColor.White;
            System.IO.File.AppendAllText("ficha_maquina.txt", '\n' + fecha.ToString("R") + " --> " + noticia);

        }
    }
}
