﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ModeloEjemplo : IModeloEjemplo
    {
        private List<Ejemplo> ejemplos;

        public ModeloEjemplo()
        {
            ejemplos = new List<Ejemplo>();
        }

        public void Crear(Ejemplo ejemplo)
        {
            ejemplos.Add(ejemplo);
        }

        public void Crear(int entero, string str)
        {
            Crear(new Ejemplo(entero, str));
        }

        public IEnumerable<Ejemplo> LeerTodos()
        {
            return ejemplos;
        }
        public Ejemplo LeerUno(string str)
        {
            foreach (Ejemplo ejemplo in ejemplos)
            {
                if (ejemplo.Str == str)
                    return ejemplo;
            }
            return null;
        }

        public void Eliminar(int entero, string str)
        {
            Console.WriteLine(ejemplos.ToString());

            for (int i  = 0; i<ejemplos.Count;i++)
            {
                if (ejemplos[i].Str.Equals(str))
                {
             
                    ejemplos.RemoveAt(i);
                }
            }
        }

        IEnumerable<Ejemplo> IModeloEjemplo.LeerTodos()
        {
            return LeerTodos();
        }

        public Ejemplo MostrarUno(string str)
        {
            for (int i = 0; i < ejemplos.Count; i++)
            {
                if (ejemplos[i].Str.Equals(str))
                {
                    return ejemplos[i];
                }
            }
            return null;

        }
        public Ejemplo MostrarUno(int entero)
        {
            for (int i = 0; i < ejemplos.Count; i++)
            {
                if (ejemplos[i].Entero == entero)
                {
                    return ejemplos[i];
                }
            }
            return null;

        }
    }
}
