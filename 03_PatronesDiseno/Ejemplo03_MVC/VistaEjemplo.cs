﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class VistaEjemplo : IVistaEjemplo
    {
        //IModeloEjemplo modelo;
        ControladorEjemplo controlador;
        public VistaEjemplo(ControladorEjemplo controlador)
        {
            this.controlador = controlador;
        }

        public void Menu()
        {
            //En bucle para alta y mostrar
            bool salir = false;
            
            while (!salir)
            {
                Console.WriteLine("------Menu-------");
                Console.WriteLine("1. Crear objeto");
                Console.WriteLine("2. Mostrar objetos");
                Console.WriteLine("3. Mostrar uno");
                Console.WriteLine("4. Eliminar objeto");
                Console.WriteLine("5. Salir");
                int opcion = int.Parse(Console.ReadLine());
                switch (opcion)
                {
                    case 1:
                        AltaEjemplo();
                        break;
                    case 2:
                        MostrarEjemplos();
                        break;
                    case 3:
                        Console.WriteLine("1. Buscar por nombre");
                        Console.WriteLine("2. Buscar por numero");
                        if(int.Parse(Console.ReadLine()) == 1)
                        {
                            Console.WriteLine("Escriba el nombre: ");
                            string nombre = Console.ReadLine();
                            controlador.MostrarUno(nombre);
                            Console.WriteLine(controlador.MostrarUno(nombre));
                        }
                        else
                        {
                            Console.WriteLine("Escriba el numero: ");
                            int numero = int.Parse(Console.ReadLine());
                            controlador.MostrarUno(numero);
                            Console.WriteLine(controlador.MostrarUno(numero));
                        }
                        break;
                    case 4:
                        Console.WriteLine("Escriba el Ejemplo(nombre y numero) a eliminar");
                        Console.Write("Numero: ");
                        int entero = int.Parse(Console.ReadLine());
                        Console.Write("Nombre: ");
                        string str = Console.ReadLine();
                        Eliminar(entero, str);
                        break;
                    case 5:
                        salir = true;
                        break;
                }
        }
    }

        public void AltaEjemplo()
        {
            Console.WriteLine("Alta ejemplo: numero: ");
            int entero = int.Parse(Console.ReadLine());
            Console.WriteLine("Alta ejemplo: STRING: ");
            string str = Console.ReadLine();
            this.controlador.AltaEjemplo(entero, str);
        }
        public void MostrarEjemplos()
        {
            //Usar controlador
            IEnumerable<Ejemplo> todos = controlador.MostrarEjemplos();
            foreach (Ejemplo ejemplo in todos)
            {
                Console.WriteLine("Ejemplo " + ejemplo.ToString());
            }
        }

        public void Eliminar(int entero, string str)
        {
            controlador.EliminarEjemplo(entero, str);
        }
        /*
public void MostrarUno(string s)
{
   Ejemplo ejemplo = modelo.LeerUno(s);
   if (ejemplo != null)
       Console.WriteLine("Ejemplo " + ejemplo.ToString());
   else
       Console.WriteLine("No encontrado por " + s);
}
*/
    }
}
