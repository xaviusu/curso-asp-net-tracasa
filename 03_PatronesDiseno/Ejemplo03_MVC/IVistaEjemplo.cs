﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IVistaEjemplo
    {
        void Menu();
        void AltaEjemplo();
        void MostrarEjemplos();
        void Eliminar(int entero, string str);

    }
}
