﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    interface IModeloEjemplo
    {
        void Crear(int entero, string str);
        IEnumerable<Ejemplo> LeerTodos();

        Ejemplo MostrarUno(string str);

        Ejemplo MostrarUno(int entero);
        void Eliminar(int entero, string str);
    }
}
