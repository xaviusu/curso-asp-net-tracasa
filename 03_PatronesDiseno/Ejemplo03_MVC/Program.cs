﻿using System;

namespace Ejemplo03_MVC
{
    class Program
    {
        IModeloEjemplo model1;
        VistaEjemplo ve;
        ControladorEjemplo controlador;
        Program()
        {
            model1 = new ModeloEjemploDiccionario();
            controlador = new ControladorEjemplo(model1);
            ve = new VistaEjemplo(controlador);

            // Instanciamos el controlador y aqui enganchamos el MVC
        }
        static void Main(string[] args)
        {
            Program program = new Program();
            program.ve.Menu();
            //program.ProbarDatos();
            // Llamar al menu
            /*ve.MostrarUno("Tres");
            ve.MostrarUno("Estres");*/
        }
       
        void ProbarDatos()
        {
            ve.AltaEjemplo();
            //model1.Crear(1, "Uno");
            //model1.Crear(2, "Dos");
            //model1.Crear(3, "Tres");

            ve.AltaEjemplo();
            ve.MostrarEjemplos();
        }
    }
}
