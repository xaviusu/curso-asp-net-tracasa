﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Ejemplo03_MVC
{
    class ControladorEjemplo
    {
        IModeloEjemplo modelo;

        public ControladorEjemplo(IModeloEjemplo modelo)
        {
            this.modelo = modelo;
        }

        public void AltaEjemplo(int entero, string str)
        {
            // Recibimos el ejemplo como parametro, probablemente de la vista
            // Guardamos el ejemplo en el modelo
            modelo.Crear(entero, str);
        }

        public void EliminarEjemplo(int entero, string str)
        {
            modelo.Eliminar(entero, str);
        }

        public IEnumerable<Ejemplo> MostrarEjemplos()
        {
            return modelo.LeerTodos();
        }

        public Ejemplo MostrarUno(string str)
        {
            return modelo.MostrarUno(str);
        }
        public Ejemplo MostrarUno(int entero)
        {
            return modelo.MostrarUno(entero);
        }
    }
}
