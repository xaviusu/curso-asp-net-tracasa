var arrayUsuarios;
class Modelo {

    constructor() {
        this.OnCreate = null;
        this.OnUpdate = null;
        this.OnDelete = null;
        this.datosIncorrectos = null;
    }
    comprobarTipo(usuario) {
        if (typeof (usuario.nombre) === "string" && !usuario.nombre == "" && isNaN(parseInt(usuario.nombre))) {
            if (!isNaN(parseInt(usuario.edad))) {
                if (!isNaN(parseFloat(usuario.altura))) {
                    return true;
                }
            }
        }
        else
            return false;
    }
    leer() {

        let promesaAjaxLeerUsuarios = fetch('http://localhost:21902/usuarios');
            promesaAjaxLeerUsuarios.catch(error => {
                    console.error("ERROR: " + error);
                })
                .then(response => response.json())
                .then(objWF => {     
                    //Callback de la llamada asincrona
                    arrayUsuarios = objWF;
                    if (arrayUsuarios !== null)
                        this.OnCreate(arrayUsuarios);
                    else
                        arrayUsuarios = [];
                });
        
    }

    guardarDatosModelo(arrayUsuarios) {
        window.localStorage.setItem("lista-usuarios", JSON.stringify(arrayUsuarios));
    }

    crear(usuario) {

        let opcionesPOST = {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                // "Content-Type": "application/x-www-form-urlencoded"
            },
            body: ` {   "nombre": "${usuario.nombre}",
                        "edad": ${usuario.edad},
                        "altura": ${usuario.altura},
                        "activo": ${usuario.activo}  }`
        };

        let promesaAJAX = fetch("http://localhost:21902/usuarios", opcionesPOST);// por defecto get
        promesaAJAX
        .then((respuesta) =>  respuesta.json())
        .then(objWF => {                // Callback de la llamada asincrona
            arrayUsuarios.push(objWF);
            this.OnCreate(arrayUsuarios);
        });
    }

    modificar(usuariomodificar, nuevousuario) {
        
        let opcionesPUT = {
            method: "PUT",
            mode: "cors",
            cache: "no-cache",
            headers: {
                "Content-Type": "application/json"
                //"Content-Type": "application/x-www-form-urlencoded"
            },
            body: `{"id": ${usuariomodificar.id},
                    "nombre": "${nuevousuario.nombre}",
                    "activo": ${nuevousuario.activo},
                    "edad": ${nuevousuario.edad},
                    "altura": ${nuevousuario.altura}
                }`
        };
        let promesaPUTModificar = fetch('http://localhost:21902/usuarios', opcionesPUT);
        promesaPUTModificar.catch(error => {
            console.error("ERROR: " + error);
        })
        .then(response => response.json())
        .then(objWF => {     
            console.log(objWF);                       //Callback de la llamada asincrona
            arrayUsuarios[arrayUsuarios.indexOf(usuariomodificar)] = objWF;
            if (arrayUsuarios !== null)
                this.OnUpdate(arrayUsuarios);
            else
                arrayUsuarios = [];
        });

    
        // this.guardarDatosModelo(arrayUsuarios);
        
    }

    eliminar(usuarioeliminar) {
        arrayUsuarios.splice(arrayUsuarios.indexOf(usuarioeliminar), 1);
        this.guardarDatosModelo(arrayUsuarios);
        this.OnDelete(arrayUsuarios);
    }

    /*
    validarUsuario(){
        let campoNombre = document.getElementById("nombre");    
        let nombre = campoNombre.value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
        if (nombre == "") {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca un nombre";
            return null;
        }
        let edad = parseInt(document.getElementById("edad").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(edad)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una edad correcta";
            return null;
        }
        let altura = parseFloat(document.getElementById("altura").value.replace(/</g, "&lt;").replace(/>/g, "&gt;"));
        if (isNaN(altura)) {
            document.getElementById("mensaje-validacion").className = "ver";
            document.getElementById("mensaje-validacion").innerHTML = "Introduzca una altura";
            return null;
        }
        let activo = document.getElementById("activo").checked;
        document.getElementById("mensaje-validacion").className = "no_ver";
        return new Usuario(nombre,edad,altura,activo);
    }*/
}