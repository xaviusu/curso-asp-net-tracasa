
class Controlador {
    usuarios = [];
    constructor() {
        //this.usuarios = JSON.parse(window.localStorage.getItem("usuarios"));
        modelo = new Modelo(this.usuarios);
        vista = new Vista(this);
        modelo.OnCreate = vista.mostrar;
        modelo.OnUpdate = vista.mostrar;
        modelo.OnDelete = vista.mostrar;
        modelo.datosIncorrectos = vista.datosIncorrectos;
    }
    crear(nombre, edad, altura, activo) {
        let usuario = new Usuario(nombre, edad, altura, activo)
        modelo.crear(usuario);
    }
/*
    devolverId() {
        return modelo.devolverId();
    }*/

    actualizarTabla() {
        return modelo.actualizarTabla();
    }
    listarUsuarios() {
        return modelo.listaUsuarios();
    }

    modificarUsuario(usuOld, usuNew) {
        modelo.modificar(usuOld, usuNew);
    }

    eliminar(usuario) {
        modelo.eliminar(usuario);
    }

}


window.onload = function() {
    controlador = new Controlador();
    modelo.leer();
    let btnAnadir = document.getElementById("btn-anadir");
    btnAnadir.addEventListener("click", vista.crear.bind(vista));
};

