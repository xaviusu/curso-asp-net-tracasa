/* JavaScript no tiene clases. Es un lenguaje prototípico. Basado en 
prototipos. */
    let modelo;
    let controlador;
    let vista;
    let modificando = false;
    let usuario;
class Vista{
    arrayUsuarios;
    usuMod;
    constructor(controlador) 
    {    
        this.controlador = controlador;
        //this.id= this.devolverId() + 1;
    }
/*
    getID(){
        return this.id
    }

    setID(id){
        this.id=id;
    }
*/
    datosIncorrectos() {
        alert("Datos incorrectos o vacios");
    }
    crear(){
        let nombre= document.getElementById("nombre").value;
        let edad= parseInt(document.getElementById("edad").value);
        let altura= parseFloat(document.getElementById("altura").value);
        let activo= document.getElementById("activo").checked;
        let idMod = document.getElementById("id").value;
        if(modificando) {
            controlador.modificarUsuario(this.usuMod, new Usuario(nombre,edad,altura,activo));
            this.usuMod = null;
            modificando=false;
        } else {
            controlador.crear(nombre,edad,altura,activo);
            //this.id++;
        }
        //this.mostrar(); // funcion callback
    }

    /*devolverId() {
        return this.controlador.devolverId();
    }*/

    modificar(usuario) {
        this.usuMod = usuario;
        document.getElementById("nombre").value = usuario.nombre;
        document.getElementById("edad").value = usuario.edad;
        document.getElementById("altura").value = usuario.altura;
        document.getElementById("activo").checked = usuario.activo;
        document.getElementById("btn-anadir").value = "Modificar";

        modificando = true;
    }


    mostrar(arrayUsuarios){
        document.getElementById("btn-anadir").value = "Añadir";
        if(arrayUsuarios == undefined) {
            arrayUsuarios = null;
            console.log("esta null");
        } else {
            
        }
        let filaTabla = "";
        //console.log(arrayUsuarios.length);
        if(arrayUsuarios!=null){
            for (let i = 0; i< arrayUsuarios.length; i++) {
                if(arrayUsuarios[i] != undefined) {
                    filaTabla +=  
            `<tr><td>${arrayUsuarios[i].nombre}</td>
             <td>${arrayUsuarios[i].edad}</td>
             <td>${arrayUsuarios[i].altura} m</td>
             <td>${arrayUsuarios[i].activo}</td>
             <td style="display:none">${arrayUsuarios[i].id} m</td>
    
             <td><input id="btn-modificar${i}" type="button" value="Modificar"/></td>
             <td><input id="btn-eliminar${i}" type="button" value="Eliminar"/></td>
    
             </tr>`
                }
                
            }
        }
        
         document.getElementById("tbody-usuarios").innerHTML = filaTabla;
         let btnEliminar = [];
         let btnModificar = []
        for (let i = 0; i< arrayUsuarios.length; i++) {
            btnEliminar[i] = document.getElementById("btn-eliminar" + i);
            btnEliminar[i].addEventListener("click", () => {vista.eliminar(arrayUsuarios[i]);});
            btnModificar[i] = document.getElementById("btn-modificar" + i);
            btnModificar[i].addEventListener("click", () => {vista.modificar(arrayUsuarios[i]);});
        }
    }
    eliminar(usuario) {
        console.log(usuario);
        controlador.eliminar(usuario);
    }
    
}
