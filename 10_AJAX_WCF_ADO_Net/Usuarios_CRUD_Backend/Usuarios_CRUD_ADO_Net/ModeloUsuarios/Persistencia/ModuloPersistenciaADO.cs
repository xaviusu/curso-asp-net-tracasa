﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Text.Json;

using System.Text.Json.Serialization;

namespace ModeloUsuarios
{
    public class ModuloPersistenciaADO
    {
        const string CONEX_BD = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=C:\\BD\\bd_usu_test.mdf;Integrated Security=True;Connect Timeout=30";
        public enum Entorno
    {
        Ninguno = 0,
        Produccion = 1,
        Desarrollo = 2,
        Preproduccion = 3,
    }

        static Entorno entorno = Entorno.Desarrollo;
        ModeloUsuario modelo;

        List<Usuario> listaUsuarios = new List<Usuario>();


        public ModuloPersistenciaADO(Entorno entorno)
        {
            ModuloPersistenciaADO.entorno = entorno;

            ModeloUsuario.Instancia.delegadoCrear = Crear;
            ModeloUsuario.Instancia.delegadoEliminar = Eliminar;
            ModeloUsuario.Instancia.leerTodo = LeerTodo;
            ModeloUsuario.Instancia.leerUno = LeerUno;
            ModeloUsuario.Instancia.leerConfirmar = LeerConfirmar;
            ModeloUsuario.Instancia.modificar = Modificar;
            listaUsuarios = Leer();
        }
        public Usuario Crear(Usuario u)
        {
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "INSERT INTO Usuario (nombre, email, altura, edad, activo) VALUES ('"
                            + u.Nombre + "', '"
                            + u.Nombre.ToLower().Replace(' ', '_')
                            + "@email.es', "
                            + u.Altura + ", "
                            + u.Edad + ", '" 
                            + u.Activo + "')";
                    int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                    {
                        throw new Exception("No ha hecho insert bbdd "
                                + comando.CommandText);

                    }
                            
                    comando.CommandText = "SELECT Usuario.Id FROM Usuario WHERE Usuario.email ='" + u.Nombre.ToLower().Replace(' ', '_') + "@email.es'";
                    int id = (int) comando.ExecuteScalar();
                    u.Id = id;
                    comando.CommandText = "SELECT Usuario.activo FROM Usuario WHERE Usuario.email ='" + u.Nombre.ToLower().Replace(' ', '_') + "@email.es'";
                    u.Activo = (bool)comando.ExecuteScalar();
                   
                }   // conexion.Close();

                listaUsuarios.Add(u);
                return u;
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }
            throw new ArgumentException ("Usuario no válido, no se ha creado");


        }


        public bool Eliminar(int id)
        {
            Usuario usuario = null;
            int? posUsuArray = null;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if(listaUsuarios[i].Id == id)
                {
                    usuario = listaUsuarios[i];
                    posUsuArray = i;
                    break;
                }
            }
            
            if (listaUsuarios.Count > posUsuArray)
            {
                try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();

                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "DELETE FROM Usuario WHERE Usuario.id = '" + id + "'";
                        int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho insert bbdd "
                                + comando.CommandText);

                    }   // conexion.Close();
                    listaUsuarios.RemoveAt((int)posUsuArray);
                    Console.WriteLine("Se ha eliminado ");
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public  List<Usuario> Leer()
        {
            listaUsuarios = new List<Usuario>();
            try
            {
                SqlConnection conexion;

                using (conexion = new SqlConnection(CONEX_BD))
                {
                    conexion.Open();

                    Usuario usuario;
                    SqlCommand comando = conexion.CreateCommand();
                    comando.CommandText = "SELECT id, nombre, altura, edad, activo FROM Usuario";
                    SqlDataReader lectorDR = comando.ExecuteReader();
                    
                    while (lectorDR.Read())
                    {
                        usuario = new Usuario();
                        usuario.Id = (int?)lectorDR[0]; 
                        usuario.Nombre = lectorDR[1].ToString();
                        usuario.Altura = (float) (double) lectorDR["altura"];
                        usuario.Edad = lectorDR.GetByte(3);
                        usuario.Activo = (bool)lectorDR["activo"];
                        listaUsuarios.Add(usuario);
                    }
                }   // conexion.Close();

            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
            }            
            return listaUsuarios;
        }

        public IList<Usuario> LeerTodo()
        {
            return listaUsuarios;
        }

        public Usuario LeerUno(int entero)
        {
            if(listaUsuarios.Count > entero)
            {
                return listaUsuarios[entero];
            } else
            {
                return null;
            }
        }

        public bool LeerConfirmar(string nombre)
        {
            int posicion = 0;
            for (int i = 0; i < listaUsuarios.Count; i++)
            {
                if (listaUsuarios[i].GetNombre() == nombre)
                {
                    posicion = i;
                    return true;
                }
            }
            return false;
        }

        public Usuario Modificar(Usuario usuAModificar) //Usuario con el Id sin cambiar pero con los datos nuevos
        {
            
            try
                {
                    SqlConnection conexion;

                    using (conexion = new SqlConnection(CONEX_BD))
                    {
                        conexion.Open();
                    string alt = (usuAModificar.Altura + "").Replace(",",".");
                        SqlCommand comando = conexion.CreateCommand();
                        comando.CommandText = "UPDATE Usuario SET Nombre = '" + usuAModificar.Nombre + "',Edad = " +usuAModificar.Edad + ", Altura = " + alt + ", Activo = '" + usuAModificar.Activo + "' WHERE Id = " + usuAModificar.Id;
                    int filasAfectadas = comando.ExecuteNonQuery();
                        if (filasAfectadas != 1)
                            throw new Exception("No ha hecho insert bbdd "
                                + comando.CommandText);

                    }   // conexion.Close();
                for (int i = 0; i < listaUsuarios.Count; i++)
                {
                    if (listaUsuarios[i].Id == usuAModificar.Id)
                    {
                        listaUsuarios[i] = usuAModificar;
                    }
                }
                Console.WriteLine("Se ha modificado ");
                return usuAModificar;
                }
                catch (Exception ex)
                {
                    Console.Error.WriteLine("Error leyendo bbdd: " + ex.Message);
                }
            throw new ArgumentException("Usuario no válido, no se ha modificado");

        }
    }
}
