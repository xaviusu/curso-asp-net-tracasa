using NUnit.Framework;
using ModeloUsuarios;
using System;
using System.Reflection;
using System.Collections.Generic;
using static ModeloUsuarios.ModuloPersistenciaADO;

namespace TestModeloUsuario
{
   public class TestsControladorYModelosADO
    {
        ModeloUsuario modelo;
        ModuloPersistenciaADO moduloPersistencia;
        Usuario primerUsuarioGlobal, segundoUsuarioGlobal, tercerUsuarioGlobal;


        void ResetearModeloYCtrl_ConReflection()
        {
            FieldInfo[] camposModelo = typeof(ModeloUsuario).GetFields(
                BindingFlags.NonPublic | BindingFlags.Static);
            foreach (FieldInfo campo in camposModelo)
            {
                if (campo.Name.Equals("instance"))
                {
                    ModeloUsuario.Instancia.ToString();
                    Console.WriteLine("Encontrado instance Modelo y eliminado");
                    campo.SetValue(null, null);
                    ModeloUsuario.Instancia.ToString();
                }
            }
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        [SetUp]
        public void Setup()
        {
            moduloPersistencia = new ModuloPersistenciaADO(Entorno.Desarrollo);
            modelo = ModeloUsuario.Instancia;
        }
        [TearDown]
        public void AlTerminarTest()
        {
            IList<Usuario> usus = modelo.LeerTodos();
            IList<int?> idsAEliminar = new List<int?>();
            foreach(Usuario usuario in usus)
            {
 
                if(usuario.Id >= primerUsuarioGlobal.Id)
                {

                    idsAEliminar.Add(usuario.Id);
                }
            }

            foreach(int id in idsAEliminar)
            {

                modelo.Eliminar(id);
            }
            ResetearModeloYCtrl_ConReflection();

        }

        void CrearUsuariosValidos()
        {
            //TODO esta funcion debe devolver el id del usuario creado que queremos modificar
            primerUsuarioGlobal = modelo.Crear(new Usuario("Xavier Usunariz", 27, 1));
            segundoUsuarioGlobal = modelo.Crear(new Usuario("Mariuxi Santin", 33, 2));
            tercerUsuarioGlobal = modelo.Crear(new Usuario("Julen Martinez", 45, 2));
            Console.WriteLine("Creados Xavier, Mariuxi, Julen");
        }

        public int? DevolverPosUsu(Usuario usu)
        {
            IList<Usuario> usus = modelo.LeerTodos();
            for (int i = 0; i < usus.Count; i++)
            {
                if (usus[i].Id == usu.Id)
                {
                    return i;
                }
            }
            return null;
        }

        [Test]
        public void TestCrearValidos()
        {
            CrearUsuariosValidos();
            Assert.AreEqual(45, modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal)).Edad);
            Assert.Null(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal) + 1));
            Assert.AreEqual(modelo.LeerTodos().Count, (int)DevolverPosUsu(tercerUsuarioGlobal) + 1, "Mal creados los usuarios");
            Assert.AreEqual(1.0f, modelo.Crear(new Usuario("German Caballero", -3, 2)).Edad);
            Assert.AreEqual("Xavier Usunariz", modelo.LeerUno((int)DevolverPosUsu(primerUsuarioGlobal)).Nombre);
            Assert.IsTrue(modelo.LeerUno((int)DevolverPosUsu(primerUsuarioGlobal)).Equals(primerUsuarioGlobal));
        }

        [Test]
        public void TestCrearInvalidos()
        {
            CrearUsuariosValidos();
            modelo.Crear(new Usuario(null, 400, 27));
            modelo.Crear(null);
            Assert.AreEqual(modelo.LeerTodos().Count, (int)DevolverPosUsu(tercerUsuarioGlobal) + 1, "Mal creados los usuarios");
        }

        [Test]
        public void TestModificar()
        {
            CrearUsuariosValidos();
            Usuario usuarioAModificar = modelo.Crear(new Usuario("German Caballero", -3, 2));
            Usuario usuModificado = new Usuario(usuarioAModificar.Id, "Xavier Usunariz", 3, 7, true);
            modelo.Modificar(usuModificado);
            Assert.AreNotEqual(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal) + 1), usuarioAModificar);
            Assert.AreEqual(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal) + 1), usuModificado);
            Usuario usuNoExiste = new Usuario("NoExisto", 88, 88);
            Assert.Throws<ArgumentException>(() => modelo.Modificar(usuNoExiste));
            

        }

        [Test]
        public void TestEliminar()
        {
            CrearUsuariosValidos();
            Usuario usu = modelo.Crear(new Usuario("German Caballero", -3, 2));
            Assert.AreEqual(true, modelo.Eliminar((int)modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal) + 1).Id));
            foreach (Usuario usuario in modelo.LeerTodos())
            {
                Assert.AreEqual(false, usuario == usu);
            }
            Assert.AreEqual((int)DevolverPosUsu(tercerUsuarioGlobal) + 1, modelo.LeerTodos().Count);
            Assert.AreEqual(false, modelo.Eliminar((int)modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal)).Id + 1));
        }

        [Test]
        public void TestLeerUno()
        {
            CrearUsuariosValidos();
            Assert.Null(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal) + 1));
            Assert.NotNull(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal)));
            Assert.IsInstanceOf<Usuario>(modelo.LeerUno((int)DevolverPosUsu(tercerUsuarioGlobal))); 
        }

        [Test]
        public void TestLeerTodos()
        {
            Assert.AreEqual(7, modelo.LeerTodos().Count);
            CrearUsuariosValidos();
            Assert.AreEqual(modelo.LeerTodos().Count, (int)DevolverPosUsu(tercerUsuarioGlobal) + 1);
            modelo.Eliminar((int)tercerUsuarioGlobal.Id);
            Assert.AreEqual((int)DevolverPosUsu(segundoUsuarioGlobal)+1, modelo.LeerTodos().Count);

        }
    }

}